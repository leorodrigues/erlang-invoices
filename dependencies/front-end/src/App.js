import React from 'react';

import PageViewer from "./components/PageViewer";
import Page from './components/Page';
import InvoiceList from "./components/InvoiceList";
import InvoiceAppMenu from './components/InvoiceAppMenu';
import StatusBar from './components/StatusBar';
import Container from 'react-bootstrap/esm/Container';
import InvoiceDisplay from './components/InvoiceDisplay';

function App() {
  return (
    <React.Fragment>
      <InvoiceAppMenu />
      <Container>
        <span>&nbsp;</span>
        <StatusBar />
        <PageViewer selectedPage={1}>
          <Page id="invoice-list-page"><InvoiceList/></Page>
          <Page id="sneak-peek-invoice-page"><InvoiceDisplay /></Page>
          <Page id="edit-invoice-page">Edit invoice</Page>
          <Page id="new-invoice-page">Edit invoice</Page>
        </PageViewer>
      </Container>
      <div className="invoices-app-footer"></div>
    </React.Fragment>
  );
}

export default App;
