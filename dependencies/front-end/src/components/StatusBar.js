import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

class StatusBar extends React.Component {
  render() {
    return (
      <Row>
        <Col>
          <Alert variant="primary">Nothing new right now!</Alert>
        </Col>
      </Row>
    );
  }
}

export default StatusBar;