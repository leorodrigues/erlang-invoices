import React from "react";
import InvoiceSection from "./InvoiceSection";
import ConditionalFieldDisplay from "./ConditionalFieldDisplay";

class InvoiceBankSection extends React.Component {
  render() {
    return (
      <InvoiceSection title={this.props.title}>

        <ConditionalFieldDisplay
          label="Name"
          fieldName="name"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Account SWIFT"
          fieldName="swiftNumber"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Account IBAN"
          fieldName="ibanNumber"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Account Number"
          fieldName="accountNumber"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Account Holder Name"
          fieldName="accountHolderName"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Account Branch Number"
          fieldName="branchNumber"
          source={this.props.bank} />

        <ConditionalFieldDisplay
          label="Complete address"
          fieldName="completeAddress"
          source={this.props.bank} />

      </InvoiceSection>
    );
  }
}

export default InvoiceBankSection;