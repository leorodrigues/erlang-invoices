import React from 'react';

import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

class TippedButton extends React.Component {
  makeTooltip() {
    return <Tooltip>{this.props.tooltipText}</Tooltip>;
  }

  render() {
    return (
      <OverlayTrigger overlay={this.makeTooltip()} variant={this.props.tooltipVariant}>
        <Button variant={this.props.buttonVariant}>{this.props.children}</Button>
      </OverlayTrigger>
    );
  }
}

export default TippedButton;