
import React from "react";


import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

class InvoiceItemListDisplay extends React.Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>Description</Col>
          <Col>Quantity</Col>
          <Col>Unit price</Col>
          <Col>Total Item price</Col>
        </Row>
        <Row>
          <Col>Software development, Sep/20/2021 - Sep/30/2021</Col>
          <Col>1</Col>
          <Col>1528,83 EUR</Col>
          <Col>1528,83 EUR</Col>
        </Row>
        <Row>
          <Col>Software development, Sep/20/2021 - Sep/30/2021</Col>
          <Col>1</Col>
          <Col>4568,48 EUR</Col>
          <Col>4586,48 EUR</Col>
        </Row>
        <Row>
          <Col>Total</Col>
          <Col>6115,31 EUR</Col>
        </Row>
        <Row>
          <Col>EUR / BRL Exchange Rate: 6,49</Col>
          <Col>39688,36 EUR</Col>
        </Row>
      </Container>
    );
  }
}

export default InvoiceItemListDisplay;