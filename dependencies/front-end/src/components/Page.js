
import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Page extends React.Component {
  render() {
    return (
      <Row><Col>{this.props.children}</Col></Row>
    );
  }
}

export default Page;