
import React from 'react';
import Stack from 'react-bootstrap/Stack';
import InvoiceCard from './InvoiceCard';

class InvoiceList extends React.Component {
  render() {
    return (
      <Stack gap={3}>
        <InvoiceCard />
        <InvoiceCard />
      </Stack>
    );
  }
}

export default InvoiceList;