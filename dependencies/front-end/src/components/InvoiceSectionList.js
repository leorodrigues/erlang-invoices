import React from "react";
import ListGroup from 'react-bootstrap/ListGroup';

class InvoiceSectionList extends React.Component {
  render() {
    return (
      <ListGroup>
        {this.props.children}
      </ListGroup>
    );
  }
}

export default InvoiceSectionList;