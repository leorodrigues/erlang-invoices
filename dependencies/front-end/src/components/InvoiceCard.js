

import React from 'react';
import Card from 'react-bootstrap/Card';

class InvoiceCard extends React.Component {
  render() {
    return (
      <Card style={{ width: '36rem' }}>
        <Card.Header>
          <Card.Title>Invoice Description Here</Card.Title>
        </Card.Header>
        <Card.Body>
          <Card.Subtitle>Total value: $99.999,00</Card.Subtitle>
          <Card.Text>Total items 1, created at Oct/10/2021</Card.Text>
          <Card.Link href="#">Peek</Card.Link>
          <Card.Link href="#">Edit</Card.Link>
          <Card.Link href="#">Remove</Card.Link>
          <Card.Link href="#">Duplicate</Card.Link>
        </Card.Body>
      </Card>
    );
  }
}

export default InvoiceCard;