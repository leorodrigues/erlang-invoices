import React from 'react';
import Container from 'react-bootstrap/Container';

class PageViewer extends React.Component {
  render() {
    const children = this.props.children || [ ];
    const selectedChild = this.props.selectedPage || 0;

    return (
      <Container>
        {children[selectedChild]}
      </Container>
    );
  }
}

export default PageViewer;