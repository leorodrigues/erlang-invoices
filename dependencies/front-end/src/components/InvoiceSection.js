import React from "react";
import ListGroup from 'react-bootstrap/ListGroup';

class InvoiceSection extends React.Component {
  render() {
    return (
      <React.Fragment>
        <ListGroup.Item variant="secondary">
          <strong>{this.props.title}</strong>
        </ListGroup.Item>
        <ListGroup.Item>
          {this.props.children}
        </ListGroup.Item>
      </React.Fragment>
    );
  }
}

export default InvoiceSection;