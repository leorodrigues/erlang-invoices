import React from "react";
import InvoiceSection from "./InvoiceSection";
import ConditionalFieldDisplay from "./ConditionalFieldDisplay";

class InvoiceCompanySection extends React.Component {
  maybeRenderContact() {
    if (!(this.props.displayContact && this.props.company)) return;
    return (
      <React.Fragment>

        <ConditionalFieldDisplay
          label="Contact name"
          fieldName="name"
          source={this.props.company.contact} />

        <ConditionalFieldDisplay
          label="Contact email"
          fieldName="email"
          source={this.props.company.contact} />

        <ConditionalFieldDisplay
          label="Contact phone"
          fieldName="phoneNumber"
          source={this.props.company.contact} />

      </React.Fragment>
    )
  }

  render() {
    return (
      <InvoiceSection title={this.props.title}>

        <ConditionalFieldDisplay
          label="Name"
          fieldName="companyName"
          source={this.props.company} />

        <ConditionalFieldDisplay
          label="Fiscal number"
          fieldName="fiscalNumber"
          source={this.props.company} />

        <ConditionalFieldDisplay
          label="Address"
          fieldName="completeAddress"
          source={this.props.company} />

        {this.maybeRenderContact()}

      </InvoiceSection>
    );
  }
}

export default InvoiceCompanySection;