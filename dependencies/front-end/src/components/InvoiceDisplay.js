
import React from 'react';

import ListGroup from 'react-bootstrap/ListGroup';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import { MdOutlineCancel } from 'react-icons/md';
import { BiMailSend } from 'react-icons/bi';
import { BsBoxArrowLeft, BsPen, BsPrinter, BsTrash } from 'react-icons/bs';
import TippedButton from './TippedButton';
import InvoiceItemListDisplay from './InvoiceItemListDisplay';
import InvoiceSection from './InvoiceSection';
import InvoiceSectionList from './InvoiceSectionList';
import InvoiceCompanySection from './InvoiceCompanySection';
import ConditionalFieldDisplay from './ConditionalFieldDisplay';
import InvoiceBankSection from './InvoiceBankSection';

const PROVIDER_COMPANY = {
  companyName: 'EPOPEIA Desenvolvimento de Sistemas LTDA',
  fiscalNumber: '43.255.805/0001-99',
  completeAddress: 'Rua Belisario Augusto, 91, Apt 105, Icaraí, Niterói - RJ - CEP 24.230-200 Brasil',
  contact: {
    name: 'Leonardo Rodrigues Teixeira',
    email: 'leonardo.epopeia@protonmail.com',
    phoneNumber: '+55 21 98539-7929'
  }
};

const CONSUMER_COMPANY = {
  companyName: 'Talkdesk Inc. Portugal Unipessoal Lda',
  fiscalNumber: 'PT514 480 343',
  completeAddress: 'Rua Pedro Nunes, Edifício IPN D, 1° Piso, módulo 1.12, Santo António dos Olivais, Coimbra, 3030-199 Coimbra'
};

const BENEFICIARY = PROVIDER_COMPANY;

const INVOICE = {
  number: '00001',
  purchaseOrderNumber: '20210000001',
  issueDate: 'Nov/15/2021',
  dueDate: 'Out/20/2021',
  paymentMethod: 'Online Remittance'
};

const INTERMEDIARY_BANK = {
  name: 'Standard Chartered Bank',
  swiftNumber: 'SCBLDEFX',
  accountNumber: '500024802',
  completeAddress: 'Frankfurt am Main, Germany'
};

const FINAL_BANK = {
  name: 'Banco Master S/A',
  swiftNumber: 'BMAXBRRJ',
  branchNumber: '0001',
  accountNumber: 'BR1933923798000000043255805C1',
  accountHolderName: 'EPOPEIA DESENVOLVIMENTO DE SISTEMAS LTDA',
  completeAddress: 'ABC - Atlantica Business Center, Floor 9, Avenida Atlantica 1130 - Rio de Janeiro - RJ, 22021-000'
};

class InvoiceDisplay extends React.Component {
  render() {
    return (
      <InvoiceSectionList>
        <InvoiceSection title="Software development services rendered to Talkdesk during Sep/2021 and Oct/2021">
          <ConditionalFieldDisplay
            label="Invoice Number"
            fieldName="number"
            source={INVOICE} />

          <ConditionalFieldDisplay
            label="PO Number"
            fieldName="purchaseOrderNumber"
            source={INVOICE} />

          <ConditionalFieldDisplay
            label="Issue date"
            fieldName="issueDate"
            source={INVOICE} />

          <ConditionalFieldDisplay
            label="Due date"
            fieldName="dueDate"
            source={INVOICE} />
        </InvoiceSection>

        <InvoiceCompanySection
          title="Provider data"
          company={PROVIDER_COMPANY} />

        <InvoiceCompanySection
          title="Consumer data"
          company={CONSUMER_COMPANY} />

        <InvoiceSection title="Services rendered">
          <InvoiceItemListDisplay />
        </InvoiceSection>

        <InvoiceSection title="Payment method">
          <ConditionalFieldDisplay
            fieldName="paymentMethod"
            source={INVOICE} />
        </InvoiceSection>

        <InvoiceBankSection
          title="Intermediary bank"
          bank={INTERMEDIARY_BANK} />

        <InvoiceBankSection
          title="Final bank"
          bank={FINAL_BANK} />

        <InvoiceCompanySection
          displayContact={true}
          title="Beneficiary"
          company={BENEFICIARY} />

        <ListGroup.Item>
          <ButtonToolbar className="float-start">
            <ButtonGroup className="me-2">
              <TippedButton tooltipText="Delete" buttonVariant="light"><BsTrash /></TippedButton>
            </ButtonGroup>
            <ButtonGroup className="me-2">
              <TippedButton tooltipText="Print" buttonVariant="light"><BsPrinter /></TippedButton>
              <TippedButton tooltipText="Edit" buttonVariant="light"><BsPen /></TippedButton>
            </ButtonGroup>

            <ButtonGroup className="me-2">
              <TippedButton tooltipText="Cancel" buttonVariant="light"><MdOutlineCancel /></TippedButton>
            </ButtonGroup>

            <ButtonGroup className="me-2">
              <TippedButton tooltipText="Submit" buttonVariant="light"><BiMailSend /></TippedButton>
            </ButtonGroup>
          </ButtonToolbar>
          <ButtonToolbar className="float-end">
            <ButtonGroup className="me-2">
              <TippedButton tooltipText="Back to listing" buttonVariant="primary"><BsBoxArrowLeft /></TippedButton>
            </ButtonGroup>
          </ButtonToolbar>
        </ListGroup.Item>
      </InvoiceSectionList>
    );
  }
}

export default InvoiceDisplay;