
import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class ConditionalFieldDisplay extends React.Component {
  renderValue = value => <Col xxl={9} xl={9} lg={9} md={8}>{value}</Col>

  maybeRenderLabel() {
    const { label } = this.props;
    if (label)
      return <Col xxl={3} xl={3} lg={3} md={4}><strong>{label}</strong></Col>;
  }

  maybeRenderField(source = { }) {
    const { fieldName } = this.props;
    const value = fieldName && source[fieldName];
    return value ? <Row>{this.maybeRenderLabel()}{this.renderValue(value)}</Row> : null;
  }

  render() {
    return this.maybeRenderField(this.props.source);
  }
}

export default ConditionalFieldDisplay;