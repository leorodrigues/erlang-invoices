%%%-------------------------------------------------------------------
%%% @author lrteixeira
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. Oct 2021 14:56
%%%-------------------------------------------------------------------
-module(invoices_crypto).
-author("lrteixeira").

%% API
-export([make_invoice_number/0]).

make_invoice_number() ->
  case invoices_database:generate_next(invoice_number) of
    {error, Reason} -> {error, Reason};
    Next -> list_to_binary(to_zero_padded_list(Next))
  end.

to_zero_padded_list(Number) ->
  io_lib:format("~6..0B", [Number]).