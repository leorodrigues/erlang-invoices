%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021,leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 14. Aug 2021 1:17 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_single_invoice).
-author("Leonardo R. Teixeira").

%% API
-export([get_paths/0, init/2, allowed_methods/2, content_types_provided/2,
  content_types_accepted/2, handle_json_request/2, resource_exists/2,
  delete_resource/2, options/2]).

-import(invoices_use_cases, [
  update_invoice/1, delete_invoice/1, find_invoice_by_id/1,
  invoice_exists_with_id/1
]).

-import(church, [chain_apply/2]).

-include("invoices_records.hrl").

get_paths() ->
  [ {"/state/invoices/:invoice_id", ?MODULE, [ ]} ].

%% -- Cowboy flow

init(Request, State) ->
  {cowboy_rest, Request, State}.

allowed_methods(Request, State) ->
  {[<<"GET">>, <<"PUT">>, <<"DELETE">>, <<"OPTIONS">>], Request, State}.

content_types_provided(Request, State) ->
  {get_provider_list(), Request, State}.

content_types_accepted(Request, State) ->
  {get_accepted_list(), Request, State}.

handle_json_request(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun get_invoice_id/1,
    fun consult_database/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]);

handle_json_request(Request = #{method := <<"PUT">>}, State) ->
  chain_apply({Request, State}, [
    fun get_invoice_id/1,
    fun get_request_body/1,
    fun parse_request_body/1,
    fun update_database/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

options(Request, State) ->
  chain_apply({ok, Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

delete_resource(Request, State) ->
  chain_apply({Request, State}, [
    fun get_invoice_id/1,
    fun run_delete_invoice/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

resource_exists(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun get_invoice_id/1,
    fun consult_invoice_exists/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]);

resource_exists(Request, State) ->
  invoices_handle_response_headers:set_response_headers({true, Request, State}).

%% -- Internals

get_provider_list() ->
  [{<<"application/json">>, handle_json_request}].

get_accepted_list() ->
  [{<<"application/json">>, handle_json_request}].

get_invoice_id({Request, State}) ->
  {binary_to_integer(cowboy_req:binding(invoice_id, Request)), {Request, State}}.

get_request_body({Id, {Request, State}}) ->
  {Id, cowboy_req:read_body(Request), State}.

consult_database({Id, State}) ->
  {find_invoice_by_id(Id), State}.

run_delete_invoice({Id, {Request, State}}) ->
  delete_invoice(Id),
  {true, Request, State}.

consult_invoice_exists({Id, {Request, State}}) ->
  {invoice_exists_with_id(Id), Request, State}.

parse_request_body({Id, {ok, Payload, Request}, State}) ->
  {invoices_invoice_rest_mapper:from_json(Payload, Id), {Request, State}}.

update_database({Invoice, State}) ->
  {update_invoice(Invoice), State}.

assemble_response({I = #invoice{ }, {Request, State}}) ->
  {invoices_invoice_rest_mapper:to_json(I), Request, State};

assemble_response({{error, Reason}, {Request, State}}) ->
  {false, make_explanation(Request, Reason), State};

assemble_response({ok, {Request, State}}) ->
  {true, Request, State}.

make_explanation(Request, Reason) ->
  cowboy_req:set_resp_body(jsx:encode(make_message(Reason)), Request).

make_message(Content) ->
  #{message => list_to_binary(Content)}.
