%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 16. Aug 2021 8:07 AM
%%%-------------------------------------------------------------------
-module(invoices_invoice_rest_mapper).
-author("Leonardo R. Teixeira").

-behaviour(gen_server).

-export([start_link/0, from_json/1, from_json/2, to_json/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-include("invoices_records.hrl").

-import(lists, [zip/2, seq/2, foldl/3]).
-import(church, [chain_apply/2]).

-type to_json_input() :: #invoice{ } | [#invoice{ }] | [].

%% @doc Transforms a json binary back into a guest record.
-spec(from_json(JsonBinary :: binary()) -> #invoice{ }).
from_json(JsonBinary) when is_binary(JsonBinary) ->
  gen_server:call(?SERVER, {from_json, JsonBinary}).

%% @doc Transforms a json binary back into a invoice record
%% overriding the id field.
-spec(from_json(JsonBinary :: binary(), Id :: pos_integer()) -> #invoice{ }).
from_json(JsonBinary, Id) when is_binary(JsonBinary) ->
  gen_server:call(?SERVER, {from_json, JsonBinary, Id}).

%% @doc Transforms a invoice record into a json binary
-spec(to_json(Invoice :: to_json_input()) -> binary()).
to_json(I = #invoice{ }) ->
  gen_server:call(?SERVER, {to_json, I});

to_json(Invoices) when is_list(Invoices) ->
  gen_server:call(?SERVER, {to_json, Invoices}).

%% @doc Spawns the server and registers the local name (unique)
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%% @private
%% @doc Initializes the server
-spec(init(Args :: term()) ->
  {ok, State :: [ ]}
  | {ok, State :: [ ], timeout() | hibernate}
  | {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, [ ]}.

%% @private
%% @doc Handling call messages
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: [ ]) ->
  {reply, Reply :: term(), NewState :: [ ]}
  | {reply, Reply :: term(), NewState :: [ ], timeout() | hibernate}
  | {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), Reply :: term(), NewState :: [ ]}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_call({from_json, JsonBinary}, _From, State) ->
  {reply, to_record(jsx:decode(JsonBinary)), State};

handle_call({from_json, JsonBinary, Id}, _From, State) ->
  {reply, to_record(override_id(jsx:decode(JsonBinary), Id)), State};

handle_call({to_json, Invoice = #invoice{ }}, _From, State) ->
  {reply, jsx:encode(to_term(Invoice, State)), State};

handle_call({to_json, Invoices}, _From, State) when is_list(Invoices) ->
  {reply, jsx:encode([to_term(I, State) || I <- Invoices]), State}.

%% @private
%% @doc Handling cast messages
-spec(handle_cast(Request :: term(), State :: [ ]) ->
  {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_cast(_Request, State) ->
  {noreply, State}.

%% @private
%% @doc Handling all non call/cast messages
-spec(handle_info(Info :: timeout() | term(), State :: [ ]) ->
  {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_info(_Info, State) ->
  {noreply, State}.

%% @private
%% @doc This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: [ ]) -> term()).
terminate(_Reason, _State) ->
  ok.

%% @private
%% @doc Convert process state when code is changed
-spec(code_change(OldVsn :: term() | {down, term()}, State :: [ ],
    Extra :: term()) ->
  {ok, NewState :: [ ]}
  | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

to_term(Invoice, _Fields) ->
  chain_apply(#{ }, [
    try_appending(<<"id">>, #invoice.id, Invoice),
    try_appending(<<"invoiceNumber">>, #invoice.invoice_number, Invoice),
    try_appending(<<"purchaseOrderNumber">>, #invoice.po_number, Invoice),
    try_appending(<<"dueDate">>, #invoice.due_date, Invoice),
    try_appending(<<"issueDate">>, #invoice.issue_date, Invoice),
    try_appending(<<"paymentMethod">>, #invoice.payment_method, Invoice),
    try_appending(<<"status">>, #invoice.status, Invoice),
    try_appending(<<"totalPrice">>, #invoice.total_price, Invoice),
    try_appending_company(<<"provider">>, #invoice.provider, Invoice),
    try_appending_company(<<"customer">>, #invoice.customer, Invoice),
    try_appending_remittance(Invoice),
    try_appending_items(Invoice),
    try_appending(<<"description">>, #invoice.description, Invoice)
  ]).

to_record(InvoiceMap) ->
  #invoice{
    id = maps:get(<<"id">>, InvoiceMap, undefined),
    invoice_number = maps:get(<<"invoiceNumber">>, InvoiceMap, undefined),
    po_number = maps:get(<<"purchaseOrderNumber">>, InvoiceMap, undefined),
    due_date = get_date(<<"dueDate">>, InvoiceMap),
    issue_date = get_date(<<"issueDate">>, InvoiceMap),
    payment_method = maps:get(<<"paymentMethod">>, InvoiceMap, undefined),
    customer = get_company(<<"customer">>, InvoiceMap),
    provider = get_company(<<"provider">>, InvoiceMap),
    remittance_info = get_remittance_info(<<"remittanceInfo">>, InvoiceMap),
    items = get_items(<<"items">>, InvoiceMap),
    description = maps:get(<<"description">>, InvoiceMap, undefined)
  }.

try_appending_items(Invoice) ->
  fun(Map) ->
    case Invoice#invoice.items of
      undefined -> Map;
      Items -> Map#{<<"items">> => [item_to_map(I) || I <- Items]}
    end
  end.

try_appending_remittance(Invoice) ->
  fun(Map) ->
    case Invoice#invoice.remittance_info of
      undefined -> Map;
      Info -> Map#{<<"remittanceInfo">> => remittance_info_to_map(Info)}
    end
  end.

try_appending_company(Key, Property, Invoice) ->
  fun(Map) ->
    case element(Property, Invoice) of
      undefined -> Map;
      Company -> Map#{ Key => company_to_map(Company) }
    end
  end.

try_appending_bank(Key, Property, RemittanceInfo) ->
  fun(Map) ->
    case element(Property, RemittanceInfo) of
      undefined -> Map;
      Bank -> Map#{ Key => bank_to_map(Bank) }
    end
  end.

try_appending_contact(Company) ->
  fun(Map) ->
    case Company#company.contact of
      undefined -> Map;
      Contact -> Map#{<<"contact">> => contact_to_map(Contact)}
    end
  end.

try_appending(Key, Property, Record) ->
  fun(Map) ->
    case element(Property, Record) of
      undefined -> Map;
      Value -> Map#{ Key => to_binary(Value) }
    end
  end.

item_to_map(Item) ->
  chain_apply(#{ }, [
    try_appending(<<"unitPrice">>, #invoice_item.unit_price, Item),
    try_appending(<<"quantity">>, #invoice_item.quantity, Item),
    try_appending(<<"description">>, #invoice_item.description, Item),
    try_appending(<<"totalPrice">>, #invoice_item.total_price, Item)
  ]).

contact_to_map(Contact) ->
  chain_apply(#{ }, [
    try_appending(<<"email">>, #contact.email, Contact),
    try_appending(<<"name">>, #contact.name, Contact)
  ]).

bank_to_map(Bank) ->
  chain_apply(#{ }, [
    try_appending(<<"completeAddress">>, #bank_info.complete_address, Bank),
    try_appending(<<"swiftNumber">>, #bank_info.swift_number, Bank),
    try_appending(<<"ibanNumber">>, #bank_info.iban_number, Bank),
    try_appending(<<"branchNumber">>, #bank_info.agency_number, Bank),
    try_appending(<<"accountHolderName">>, #bank_info.account_holder_name, Bank),
    try_appending(<<"accountNumber">>, #bank_info.account_number, Bank)
  ]).

remittance_info_to_map(Info) ->
  chain_apply(#{ }, [
    try_appending_bank(<<"intermediaryBank">>, #remittance_info.intermediary_bank, Info),
    try_appending_bank(<<"finalBank">>, #remittance_info.final_bank, Info)
  ]).

company_to_map(Company) ->
  chain_apply(#{ }, [
    try_appending(<<"completeAddress">>, #company.complete_address, Company),
    try_appending(<<"tradingName">>, #company.trading_name, Company),
    try_appending(<<"companyName">>, #company.company_name, Company),
    try_appending(<<"fiscalNumber">>, #company.fiscal_number, Company),
    try_appending_contact(Company)
  ]).

override_id(JsonMap, Id) -> JsonMap#{<<"id">> => Id}.

get_company(Key, InvoiceMap) ->
  get_and_transform(Key, InvoiceMap, fun transform_company/1).

get_remittance_info(Key, InvoiceMap) ->
  get_and_transform(Key, InvoiceMap, fun transform_remittance_info/1).

get_items(Key, InvoiceMap) ->
  get_and_transform(Key, InvoiceMap, fun transform_item_map_list/1).

get_bank(Key, RemittanceMap) ->
  get_and_transform(Key, RemittanceMap, fun transform_bank/1).

get_contact(Key, CompanyMap) ->
  get_and_transform(Key, CompanyMap, fun transform_contact/1).

get_date(Key, InvoiceMap) ->
  get_and_transform(Key, InvoiceMap, fun transform_date/1).

transform_date(DateBinary) ->
  chain_apply(DateBinary, [
    fun remove_final_zed/1,
    fun split_date_and_time/1,
    fun split_date_and_time_parts/1,
    fun make_date_time_tuple/1
  ]).

transform_company(CompanyMap) ->
  #company{
    company_name = maps:get(<<"companyName">>, CompanyMap, undefined),
    complete_address = maps:get(<<"completeAddress">>, CompanyMap, undefined),
    trading_name = maps:get(<<"tradingName">>, CompanyMap, undefined),
    contact = get_contact(<<"contact">>, CompanyMap),
    fiscal_number = maps:get(<<"fiscalNumber">>, CompanyMap, undefined)
  }.

transform_remittance_info(RemittanceMap) ->
  #remittance_info{
    final_bank = get_bank(<<"finalBank">>, RemittanceMap),
    intermediary_bank = get_bank(<<"intermediaryBank">>, RemittanceMap)
  }.

transform_item_map_list(ItemMapList) ->
  [transform_item(ItemMap) || ItemMap <- ItemMapList].

transform_item(ItemMap) ->
  #invoice_item{
    description = maps:get(<<"description">>, ItemMap, undefined),
    quantity = maps:get(<<"quantity">>, ItemMap, undefined),
    unit_price = maps:get(<<"unitPrice">>, ItemMap, undefined)
  }.

transform_contact(ContactMap) ->
  #contact{
    email = maps:get(<<"email">>, ContactMap, undefined),
    name = maps:get(<<"name">>, ContactMap, undefined)
  }.

transform_bank(BankMap) ->
  #bank_info{
    account_number = maps:get(<<"accountNumber">>, BankMap, undefined),
    account_holder_name = maps:get(<<"accountHolderName">>, BankMap, undefined),
    agency_number = maps:get(<<"branchNumber">>, BankMap, undefined),
    iban_number = maps:get(<<"ibanNumber">>, BankMap, undefined),
    swift_number = maps:get(<<"swiftNumber">>, BankMap, undefined),
    complete_address = maps:get(<<"completeAddress">>, BankMap, undefined)
  }.

get_and_transform(Key, Map, Transform) ->
  case maps:get(Key, Map, undefined) of
    undefined -> undefined;
    Value -> apply(Transform, [Value])
  end.

remove_final_zed(DateBinary) ->
  binary:replace(DateBinary, <<"Z">>, <<"">>).

split_date_and_time(DateBinary) ->
  binary:split(DateBinary, <<"T">>).

split_date_and_time_parts([DateBinary, TimeBinary]) ->
  {
    binary:split(DateBinary, <<"-">>, [global]),
    binary:split(TimeBinary, <<":">>, [global])
  }.

make_date_time_tuple({DatePart, TimePart}) ->
  {time_list_to_tuple(DatePart), time_list_to_tuple(TimePart)}.

time_list_to_tuple([A, B, C]) ->
  {binary_to_integer(A), binary_to_integer(B), binary_to_integer(C)}.

to_binary(Value) when is_atom(Value) -> atom_to_binary(Value);
to_binary(Value) -> Value.
