%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Aug 2021 11:53 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_api_docs).
-author("Leonardo R. Teixeira").

%% API
-export([get_paths/0, init/2]).

get_paths() ->
  [
    {"/docs/LICENSE", cowboy_static, as_text_plain_file("LICENSE")},
    {"/docs/ui/[...]", cowboy_static, {priv_dir, invoices_app, "swagger-ui"}},
    {"/docs/ui", ?MODULE, [ ]}
  ].

init(Request, State) ->
  {ok, cowboy_req:reply(302, get_location(), <<>>, Request), State}.

get_location() ->
  #{
    <<"location">> => <<"/docs/ui/index.html">>,
    <<"cache-control">> => <<"no-cache">>
  }.

as_text_plain_file(Path) ->
  {priv_file, invoices_app, Path, [
    {mimetypes, {<<"text">>, <<"plain">>, []}}
  ]}.
