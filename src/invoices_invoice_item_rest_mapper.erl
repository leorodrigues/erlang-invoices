%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 16. Aug 2021 8:07 AM
%%%-------------------------------------------------------------------
-module(invoices_invoice_item_rest_mapper).
-author("Leonardo R. Teixeira").

-behaviour(gen_server).

-export([start_link/0, from_json/1, to_json/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-include("invoices_records.hrl").

-import(lists, [zip/2, seq/2, foldl/3]).
-import(church, [chain_apply/2]).

-type to_json_input() :: #invoice_item{ } | [#invoice_item{ }] | [].

%% @doc Transforms a json binary back into a guest record.
-spec(from_json(JsonBinary :: binary()) -> #invoice{ }).
from_json(JsonBinary) when is_binary(JsonBinary) ->
  gen_server:call(?SERVER, {from_json, JsonBinary}).

%% @doc Transforms a invoice record into a json binary
-spec(to_json(InvoiceItem :: to_json_input()) -> binary()).
to_json(I = #invoice_item{ }) ->
  gen_server:call(?SERVER, {to_json, I});

to_json(Invoices) when is_list(Invoices) ->
  gen_server:call(?SERVER, {to_json, Invoices}).

%% @doc Spawns the server and registers the local name (unique)
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%% @private
%% @doc Initializes the server
-spec(init(Args :: term()) ->
  {ok, State :: [ ]}
  | {ok, State :: [ ], timeout() | hibernate}
  | {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, [ ]}.

%% @private
%% @doc Handling call messages
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: [ ]) ->
  {reply, Reply :: term(), NewState :: [ ]}
  | {reply, Reply :: term(), NewState :: [ ], timeout() | hibernate}
  | {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), Reply :: term(), NewState :: [ ]}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_call({from_json, JsonBinary}, _From, State) ->
  {reply, transform_item(jsx:decode(JsonBinary)), State};

handle_call({to_json, InvoiceItem = #invoice_item{ }}, _From, State) ->
  {reply, jsx:encode(item_to_map(InvoiceItem)), State};

handle_call({to_json, InvoiceItems}, _From, State) when is_list(InvoiceItems) ->
  {reply, jsx:encode([item_to_map(I) || I <- InvoiceItems]), State}.

%% @private
%% @doc Handling cast messages
-spec(handle_cast(Request :: term(), State :: [ ]) ->
  {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_cast(_Request, State) ->
  {noreply, State}.

%% @private
%% @doc Handling all non call/cast messages
-spec(handle_info(Info :: timeout() | term(), State :: [ ]) ->
  {noreply, NewState :: [ ]}
  | {noreply, NewState :: [ ], timeout() | hibernate}
  | {stop, Reason :: term(), NewState :: [ ]}).
handle_info(_Info, State) ->
  {noreply, State}.

%% @private
%% @doc This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: [ ]) -> term()).
terminate(_Reason, _State) ->
  ok.

%% @private
%% @doc Convert process state when code is changed
-spec(code_change(OldVsn :: term() | {down, term()}, State :: [ ],
    Extra :: term()) ->
  {ok, NewState :: [ ]}
  | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

item_to_map(InvoiceItem) ->
  chain_apply(#{ }, [
    try_appending(<<"unitPrice">>, #invoice_item.unit_price, InvoiceItem),
    try_appending(<<"quantity">>, #invoice_item.quantity, InvoiceItem),
    try_appending(<<"description">>, #invoice_item.description, InvoiceItem),
    try_appending(<<"totalPrice">>, #invoice_item.total_price, InvoiceItem)
  ]).

try_appending(Key, Property, Record) ->
  fun(Map) ->
    case element(Property, Record) of
      undefined -> Map;
      Value -> Map#{ Key => to_binary(Value) }
    end
  end.

transform_item(ItemMap) ->
  #invoice_item{
    description = maps:get(<<"description">>, ItemMap, undefined),
    quantity = maps:get(<<"quantity">>, ItemMap, undefined),
    unit_price = maps:get(<<"unitPrice">>, ItemMap, undefined)
  }.

to_binary(Value) when is_atom(Value) -> atom_to_binary(Value);
to_binary(Value) -> Value.
