%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 07. Oct 2021 20:04
%%%-------------------------------------------------------------------
-module(invoices_handle_invoice_item_collection).
-author("Leonardo R. Teixeira").

-export([get_paths/0, init/2, allowed_methods/2, handle_json_request/2,
  content_types_provided/2, content_types_accepted/2, resource_exists/2,
  options/2]).

-import(church, [chain_apply/2]).
-import(invoices_use_cases, [
  find_all_invoice_items/1, create_invoice_item/2
]).

get_paths() ->
  [ {"/state/invoices/:invoice_id/items", ?MODULE, [ ]} ].

init(Request, State) ->
  {cowboy_rest, Request, State}.

allowed_methods(Request, State) ->
  {[<<"GET">>, <<"POST">>, <<"OPTIONS">>], Request, State}.

content_types_provided(Request, State) ->
  {get_provider_list(), Request, State}.

content_types_accepted(Request, State) ->
  {get_accepted_list(), Request, State}.

resource_exists(Request = #{method := <<"POST">>}, State) ->
  {false, Request, State};

resource_exists(Request, State) ->
  {true, Request, State}.

handle_json_request(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun retrieve_invoice_items/1,
    fun map_invoice_items_to_json/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]);

handle_json_request(Request = #{method := <<"POST">>}, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun read_request_body/1,
    fun parse_request_body/1,
    fun invoke_create_invoice/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

options(Request, State) ->
  chain_apply({ok, Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

%% -- Internals

parse_invoice_id({Request, State}) ->
  {binary_to_integer(cowboy_req:binding(invoice_id, Request)), {Request, State}}.

retrieve_invoice_items({InvoiceId, State}) ->
  {find_all_invoice_items(InvoiceId), State}.

map_invoice_items_to_json({InvoiceItems, State}) ->
  {invoices_invoice_item_rest_mapper:to_json(InvoiceItems), State}.

read_request_body({InvoiceId, {Request, State}}) ->
  {InvoiceId, {cowboy_req:read_body(Request), State}}.

parse_request_body({InvoiceId, {{ok, Body, Request}, State}}) ->
  {InvoiceId, invoices_invoice_item_rest_mapper:from_json(Body), {Request, State}}.

invoke_create_invoice({InvoiceId, NewInvoiceItem, State}) ->
  {InvoiceId, create_invoice_item(InvoiceId, NewInvoiceItem), State}.

assemble_response({InvoiceId, ItemIndex, {Request, State}})
  when is_integer(InvoiceId) and is_integer(ItemIndex) ->
  {{true, make_location_path(InvoiceId, ItemIndex)}, Request, State};

assemble_response({_, {error, Reason}, {Request, State}}) ->
  {false, make_explanation(Request, Reason), State};

assemble_response({{error, Reason}, {Request, State}}) ->
  {false, make_explanation(Request, Reason), State};

assemble_response({Payload, {Request, State}}) ->
  {Payload, Request, State}.

get_provider_list() ->
  [{<<"application/json">>, handle_json_request}].

get_accepted_list() ->
  [{<<"application/json">>, handle_json_request}].

make_location_path(InvoiceId, ItemIndex)
  when is_integer(InvoiceId) and is_integer(ItemIndex) ->
  io_lib:format("/state/invoices/~p/items/~p", [InvoiceId, ItemIndex]).

make_explanation(Request, Reason) ->
  cowboy_req:set_resp_body(jsx:encode(make_message(Reason)), Request).

make_message(Content) ->
  #{message => list_to_binary(Content)}.