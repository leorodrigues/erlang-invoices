%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021,leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 14. Aug 2021 1:17 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_single_invoice_item).
-author("Leonardo R. Teixeira").

%% API
-export([get_paths/0, init/2, allowed_methods/2, content_types_provided/2,
  content_types_accepted/2, handle_json_request/2, resource_exists/2,
  delete_resource/2, options/2]).

-import(invoices_use_cases, [
  update_invoice_item/3, delete_invoice_item/2, find_invoice_item_by_index/2,
  invoice_item_exists_at/2
]).

-import(invoices_invoice_item_rest_mapper, [from_json/1, to_json/1]).

-import(church, [chain_apply/2]).

-include("invoices_records.hrl").

get_paths() ->
  [ {"/state/invoices/:invoice_id/items/:item_index", ?MODULE, [ ]} ].

%% -- Cowboy flow

init(Request, State) ->
  {cowboy_rest, Request, State}.

allowed_methods(Request, State) ->
  {[<<"GET">>, <<"PUT">>, <<"DELETE">>, <<"OPTIONS">>], Request, State}.

content_types_provided(Request, State) ->
  {get_provider_list(), Request, State}.

content_types_accepted(Request, State) ->
  {get_accepted_list(), Request, State}.

handle_json_request(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun parse_invoice_item_index/1,
    fun retrieve_invoice_item/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]);

handle_json_request(Request = #{method := <<"PUT">>}, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun parse_invoice_item_index/1,
    fun read_request_body/1,
    fun parse_request_body/1,
    fun replace_invoice_item/1,
    fun assemble_response/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

options(Request, State) ->
  chain_apply({ok, Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

delete_resource(Request, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun parse_invoice_item_index/1,
    fun remove_invoice_item/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]).

resource_exists(Request = #{method := <<"GET">>}, State) ->
  chain_apply({Request, State}, [
    fun parse_invoice_id/1,
    fun parse_invoice_item_index/1,
    fun consult_existence_of_invoice_item/1,
    fun invoices_handle_response_headers:set_response_headers/1
  ]);

resource_exists(Request, State) ->
  invoices_handle_response_headers:set_response_headers({true, Request, State}).

%% -- Internals

get_provider_list() ->
  [{<<"application/json">>, handle_json_request}].

get_accepted_list() ->
  [{<<"application/json">>, handle_json_request}].

parse_invoice_id({Request, State}) ->
  {get_binding(invoice_id, Request), Request, State}.

parse_invoice_item_index({InvoiceId, Request, State}) ->
  {InvoiceId, get_binding(item_index, Request), Request, State}.

read_request_body({InvoiceId, ItemIndex, Request, State}) ->
  {InvoiceId, ItemIndex, cowboy_req:read_body(Request), State}.

parse_request_body({InvoiceId, ItemIndex, {ok, Payload, Request}, State}) ->
  {InvoiceId, ItemIndex, from_json(Payload), Request, State}.

retrieve_invoice_item({InvoiceId, ItemIndex, Request, State}) ->
  {find_invoice_item_by_index(InvoiceId, ItemIndex), Request, State}.

replace_invoice_item({InvoiceId, ItemIndex, InvoiceItem, Request, State}) ->
  {update_invoice_item(InvoiceId, ItemIndex, InvoiceItem), Request, State}.

remove_invoice_item({InvoiceId, ItemIndex, Request, State}) ->
  delete_invoice_item(InvoiceId, ItemIndex),
  {true, Request, State}.

consult_existence_of_invoice_item({InvoiceId, ItemIndex, Request, State}) ->
  {invoice_item_exists_at(InvoiceId, ItemIndex), Request, State}.

assemble_response({I = #invoice_item{ }, Request, State}) ->
  {to_json(I), Request, State};

assemble_response({{error, Reason}, Request, State}) ->
  {break, {false, make_explanation(Request, Reason), State}};

assemble_response({ok, Request, State}) ->
  {true, Request, State}.

make_explanation(Request, Reason) ->
  cowboy_req:set_resp_body(jsx:encode(make_message(Reason)), Request).

make_message(Content) ->
  #{message => list_to_binary(Content)}.

get_binding(Identifier, Request) ->
  binary_to_integer(cowboy_req:binding(Identifier, Request)).