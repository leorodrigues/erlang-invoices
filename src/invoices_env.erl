%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Aug 2021 8:39 PM
%%%-------------------------------------------------------------------
-module(invoices_env).
-author("leonardo").

-define(DEFAULT_HTTP_PORT, 8080).

%% API
-export([get_http_port/1, get_http_port/0]).

get_http_port() ->
  get_http_port(?DEFAULT_HTTP_PORT).

get_http_port(DefaultPort) ->
  case application:get_env(invoices_app, http_port) of
    { ok, Port } -> Port;
    _ -> DefaultPort
  end.