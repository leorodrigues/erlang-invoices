%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2021 2:07 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_rpc_duplicate_invoice).
-author("Leonardo R. Teixeira").

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).

-define(NOT_FOUND_TOKEN, "not-found").

-define(DUPLICATE_CREATED_MSG, "Duplicate Invoice Created.").

-define(LOCATION_TEMPLATE, "/state/invoices/~p").

-define(INVOICE_NOT_FOUND_MSG, "The invoice with id ~p was not found").

-define(INTERNAL_ERROR_BODY, <<"{\"message\":\"Internal Server Error\"}">>).

-define(HEADERS, #{
  <<"content-type">> => <<"application/json">>
}).

-define(MISSING_INVOICE_ID, {
  bad_request, {"Missing argument.", "missing-invoice-id"}
}).

-define(MISSING_ARGUMENTS, {
  bad_request, {"Malformed payload.", "missing-arguments-map"}
}).

get_paths() ->
  [ {"/behaviour/duplicate-invoice", ?MODULE, [ ]} ].

init(Request = #{method := <<"OPTIONS">>}, State) ->
  chain_apply({no_content, Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun assemble_response_payload/1
  ]);

init(Request, State) ->
  chain_apply({Request, State}, [
    fun parse_rpc_payload/1,
    fun run_business_logic/1,
    fun log_internal_errors/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun assemble_response_payload/1
  ]).

run_business_logic(Arguments) ->
  chain_apply(Arguments, [
    fun unwrap_arguments/1,
    fun get_invoice_id/1,
    fun try_duplicating_invoice/1
  ]).

parse_rpc_payload({Request, State}) ->
  decode_payload(cowboy_req:read_body(Request), State).

decode_payload({ok, Payload, Request}, State) ->
  {jsx:decode(Payload), Request, State}.

unwrap_arguments({JsonMap, Request, State}) ->
  case maps:get(<<"arguments">>, JsonMap, undefined) of
    undefined -> {break, {?MISSING_ARGUMENTS, Request, State}};
    Arguments -> {Arguments, Request, State}
  end.

get_invoice_id({Arguments, Request, State}) ->
  case maps:get(<<"invoiceId">>, Arguments, undefined) of
    undefined -> {break, {?MISSING_INVOICE_ID, Request, State}};
    InvoiceId -> {InvoiceId, Request, State}
  end.

try_duplicating_invoice({InvoiceId, Request, State}) ->
  case invoices_use_cases:duplicate_invoice(InvoiceId) of
    NewInvoiceId when is_integer(NewInvoiceId) ->
      {{created, explain_created(NewInvoiceId)}, Request, State};
    not_found ->
      {{not_found, explain_id_not_found(InvoiceId)}, Request, State};
    {error, Reason} ->
      {{internal_error, Reason}, Request, State}
  end.

explain_id_not_found(InvoiceId) ->
  {io_lib:format(?INVOICE_NOT_FOUND_MSG, [InvoiceId]), ?NOT_FOUND_TOKEN}.

explain_created(NewInvoiceId) ->
  {?DUPLICATE_CREATED_MSG, io_lib:format(?LOCATION_TEMPLATE, [NewInvoiceId])}.

log_internal_errors({{internal_error, Reason} = Payload, Request, State}) ->
  lager:error(Reason), {Payload, Request, State};

log_internal_errors({Payload, Request, State}) ->
  {Payload, Request, State}.

assemble_response_payload({{created, BodyData}, Request, State}) ->
  {ok, reply(200, ?HEADERS, make_location_body(BodyData), Request), State};

assemble_response_payload({no_content, Request, State}) ->
  {ok, reply(204, #{ }, <<>>, Request), State};

assemble_response_payload({{bad_request, BodyData}, Request, State}) ->
  {ok, reply(400, ?HEADERS, make_token_body(BodyData), Request), State};

assemble_response_payload({{not_found, BodyData}, Request, State}) ->
  {ok, reply(404, ?HEADERS, make_token_body(BodyData), Request), State};

assemble_response_payload({{internal_error, _}, Request, State}) ->
  {ok, reply(500, ?HEADERS, ?INTERNAL_ERROR_BODY, Request), State}.

make_token_body({Message, Token}) ->
  jsx:encode(#{
    <<"message">> => list_to_binary(Message),
    <<"token">> => list_to_binary(Token)
  }).

make_location_body({Message, Url}) ->
  jsx:encode(#{
    <<"message">> => list_to_binary(Message),
    <<"location">> => list_to_binary(Url)
  }).

reply(Status, Headers, Body, Request) ->
  cowboy_req:reply(Status, Headers, Body, Request).