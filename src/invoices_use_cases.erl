%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.org
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(invoices_use_cases).

-behaviour(gen_server).

-export([start_link/0, stop/0, create_invoice/1, update_invoice/1,
  delete_invoice/1, find_all_invoices/0, find_invoice_by_id/1,
  create_invoice_item/2, update_invoice_item/3, delete_invoice_item/2,
  find_all_invoice_items/1, find_invoice_item_by_index/2, submit_invoice/2,
  cancel_invoice/1, duplicate_invoice/1, empty_invoice/1,
  invoice_exists_with_id/1, invoice_item_exists_at/2]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-import(church, [chain_apply/2]).

-include("invoices_records.hrl").

-define(SERVER, ?MODULE).
-define(INVOICE_ID_INT_GT_ZERO, "Invoice id must be an integer greater than zero.").

create_invoice(I = #invoice{ }) ->
  gen_server:call(?SERVER, {create, I}).

update_invoice(I = #invoice{ }) ->
  gen_server:call(?SERVER, {update, I}).

delete_invoice(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, {delete, Id}).

find_all_invoices() ->
  gen_server:call(?SERVER, find_all_invoices).

find_invoice_by_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, {find_invoice_by_id, Id}).

invoice_exists_with_id(Id) when is_integer(Id) ->
  gen_server:call(?SERVER, {invoice_exists_with_id, Id}).

create_invoice_item(Id, I = #invoice_item{ }) when is_integer(Id) ->
  gen_server:call(?SERVER, {create_invoice_item, Id, I}).

update_invoice_item(Id, Index, I = #invoice_item{ })
  when is_integer(Id) and is_integer(Index) ->
  gen_server:call(?SERVER, {update_invoice_item, Id, Index, I}).

delete_invoice_item(Id, Index) when is_integer(Id) and is_integer(Index) ->
  gen_server:call(?SERVER, {delete_invoice_item, Id, Index}).

find_all_invoice_items(InvoiceId) when is_integer(InvoiceId) ->
  gen_server:call(?SERVER, {find_all_invoice_items, InvoiceId}).

find_invoice_item_by_index(InvoiceId, ItemIndex)
  when is_integer(InvoiceId) and is_integer(ItemIndex) ->
  gen_server:call(?SERVER, {find_invoice_item_by_index, InvoiceId, ItemIndex}).

invoice_item_exists_at(InvoiceId, ItemIndex)
  when is_integer(InvoiceId) and is_integer(ItemIndex) ->
  gen_server:call(?SERVER, {invoice_item_exists_at, InvoiceId, ItemIndex}).

submit_invoice(InvoiceId, PONumber) ->
  gen_server:call(?SERVER, {submit_invoice, InvoiceId, PONumber}).

duplicate_invoice(InvoiceId) ->
  gen_server:call(?SERVER, {duplicate_invoice, InvoiceId}).

cancel_invoice(InvoiceId) ->
  gen_server:call(?SERVER, {cancel_invoice, InvoiceId}).

empty_invoice(InvoiceId) ->
  gen_server:call(?SERVER, {empty_invoice, InvoiceId}).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stop() ->
  gen_server:stop(?SERVER).

init([]) ->
  {ok, [ ]}.

handle_call(find_all_invoices, _From, State) ->
  {reply, handle_find_all_invoices(), State};

handle_call({find_invoice_by_id, Id}, _From, State) when is_integer(Id) ->
  {reply, handle_find_invoice_by_id(Id), State};

handle_call({invoice_exists_with_id, Id}, _From, State) when is_integer(Id) ->
  {reply, handle_invoice_exists_with_id(Id), State};

handle_call({create, I = #invoice{ }}, _From, State) ->
  {reply, handle_create_invoice(I), State};

handle_call({update, I = #invoice{ }}, _From, State) ->
  {reply, handle_update_invoice(I), State};

handle_call({delete, Id}, _From, State) when is_integer(Id) ->
  {reply, handle_delete_invoice(Id), State};

handle_call({create_invoice_item, Id, I = #invoice_item{ }}, _From, State) ->
  {reply, handle_create_invoice_item(Id, I), State};

handle_call({update_invoice_item, Id, Index, I = #invoice_item{ }}, _From, State) ->
  {reply, handle_update_invoice_item(Id, Index, I), State};

handle_call({delete_invoice_item, Id, Index}, _From, State) ->
  {reply, handle_delete_invoice_item(Id, Index), State};

handle_call({find_all_invoice_items, InvoiceId}, _From, State) ->
  {reply, handle_find_all_invoice_items(InvoiceId), State};

handle_call({find_invoice_item_by_index, InvoiceId, ItemIndex}, _From, State) ->
  {reply, handle_find_invoice_item_by_index(InvoiceId, ItemIndex), State};

handle_call({invoice_item_exists_at, InvoiceId, ItemIndex}, _From, State) ->
  {reply, handle_invoice_item_exists_at(InvoiceId, ItemIndex), State};

handle_call({submit_invoice, InvoiceId, PONumber}, _From, State) ->
  {reply, handle_submit_invoice(InvoiceId, PONumber), State};

handle_call({duplicate_invoice, InvoiceId}, _From, State) ->
  {reply, handle_duplicate_invoice(InvoiceId), State};

handle_call({cancel_invoice, InvoiceId}, _From, State) ->
  {reply, handle_cancel_invoice(InvoiceId), State};

handle_call({empty_invoice, InvoiceId}, _From, State) ->
  {reply, handle_empty_invoice(InvoiceId), State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_find_all_invoices() ->
  invoices_database:find_all_invoices().

handle_find_invoice_by_id(Id) ->
  invoices_database:find_invoice_by_id(Id).

handle_invoice_exists_with_id(Id) ->
  invoices_database:invoice_exists_with_id(Id).

handle_find_all_invoice_items(InvoiceId) ->
  chain_apply(InvoiceId, [
    fun load_invoice/1,
    fun select_invoice_items/1
  ]).

handle_find_invoice_item_by_index(InvoiceId, ItemIndex) ->
  chain_apply({InvoiceId, ItemIndex}, [
    fun load_invoice/1,
    fun select_invoice_item_at_index/1
  ]).

handle_create_invoice(Invoice) ->
  chain_apply(Invoice, [
    fun switch_invoice_to_created/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:insert_invoice/1
  ]).

handle_update_invoice(Invoice) ->
  chain_apply(Invoice, [
    fun confirm_invoice_may_be_updated/1,
    fun confirm_compliant_invoice_id/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:update_invoice/1
  ]).

handle_delete_invoice(Id) ->
  chain_apply(Id, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_deleted/1,
    fun invoices_database:delete_invoice/1
  ]).

handle_create_invoice_item(Id, Item) ->
  chain_apply({Id, Item}, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_updated/1,
    fun append_item_to_invoice/1,
    fun save_new_invoice_item/1
  ]).

handle_update_invoice_item(Id, Index, Item) ->
  chain_apply({Id, {Index, Item}}, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_updated/1,
    fun confirm_invoice_item_exists/1,
    fun replace_item_on_invoice/1,
    fun confirm_compliant_invoice_id/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:update_invoice/1
  ]).

handle_delete_invoice_item(Id, Index) ->
  chain_apply({Id, Index}, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_updated/1,
    fun confirm_invoice_item_exists/1,
    fun remove_item_from_invoice/1,
    fun confirm_compliant_invoice_id/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:update_invoice/1
  ]).

handle_invoice_item_exists_at(InvoiceId, ItemIndex) ->
  chain_apply({InvoiceId, ItemIndex}, [
    fun load_invoice/1,
    fun was_invoice_item_found/1
  ]).

handle_submit_invoice(InvoiceId, PONumber) ->
  chain_apply({InvoiceId, PONumber}, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_submitted/1,
    fun switch_invoice_to_submitted/1,
    fun confirm_compliant_invoice_id/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:update_invoice/1
  ]).

handle_cancel_invoice(InvoiceId) ->
  chain_apply(InvoiceId, [
    fun load_invoice/1,
    fun switch_invoice_to_cancelled/1,
    fun invoices_database:update_invoice/1
  ]).

handle_empty_invoice(InvoiceId) ->
  chain_apply(InvoiceId, [
    fun load_invoice/1,
    fun confirm_invoice_may_be_emptied/1,
    fun remove_invoice_items/1,
    fun invoices_database:update_invoice/1
  ]).

handle_duplicate_invoice(InvoiceId) ->
  chain_apply(InvoiceId, [
    fun load_invoice/1,
    fun clear_invoice_identifiers/1,
    fun switch_invoice_to_created/1,
    fun compute_all_item_prices/1,
    fun compute_invoice_price/1,
    fun invoices_database:insert_invoice/1
  ]).

load_invoice({Id, State}) when is_integer(Id) ->
  case invoices_database:find_invoice_by_id(Id) of
    not_found -> {break, not_found};
    {error, _} = Error -> {break, Error};
    Invoice -> {Invoice, State}
  end;

load_invoice(InvoiceId) when is_integer(InvoiceId) ->
  case invoices_database:find_invoice_by_id(InvoiceId) of
    not_found -> {break, not_found};
    {error, _} = Error -> {break, Error};
    Invoice -> Invoice
  end.

confirm_invoice_may_be_submitted({#invoice{status = Status} = I, PONumber}) ->
  case Status of
    submitted -> {break, {error, invoice_already_submitted}};
    cancelled -> {break, {error, invoice_cancelled}};
    created -> {I, PONumber}
  end.

confirm_invoice_may_be_updated({#invoice{status = Status} = I, State}) ->
  case Status of
    submitted -> {break, {error, invoice_submitted}};
    cancelled -> {break, {error, invoice_cancelled}};
    _ -> {I, State}
  end;

confirm_invoice_may_be_updated(#invoice{status = Status} = I) ->
  case Status of
    submitted -> {break, {error, invoice_submitted}};
    cancelled -> {break, {error, invoice_cancelled}};
    _ -> I
  end.

confirm_invoice_may_be_deleted(#invoice{status = Status, id = Id}) ->
  case Status of
    submitted -> {break, {error, invoice_submitted}};
    cancelled -> {break, {error, invoice_cancelled}};
    _ -> Id
  end.

confirm_invoice_may_be_emptied(#invoice{status = Status} = I) ->
  case Status of
    submitted -> {break, {error, invoice_submitted}};
    cancelled -> {break, {error, invoice_cancelled}};
    _ -> I
  end.

confirm_compliant_invoice_id(#invoice{id = Id} = I) ->
  if
    not is_integer(Id) -> {break, {error, ?INVOICE_ID_INT_GT_ZERO}};
    Id =< 0 -> {break, {error, ?INVOICE_ID_INT_GT_ZERO}};
    true -> I
  end.

confirm_invoice_item_exists({#invoice{items = undefined}, _State}) ->
  {break, ok};

confirm_invoice_item_exists({#invoice{ } = I, {Index, InvoiceItem}}) ->
  case length(I#invoice.items) - Index of
    D when D >= 0 -> {I, Index, InvoiceItem};
    D when D < 0 -> {break, ok}
  end;

confirm_invoice_item_exists({#invoice{ } = I, Index}) ->
  case length(I#invoice.items) - Index of
    D when D >= 0 -> {I, Index};
    D when D < 0 -> {break, ok}
  end.

was_invoice_item_found({#invoice{items = undefined}, _ItemIndex}) ->
  {break, false};

was_invoice_item_found({#invoice{ } = I, ItemIndex}) ->
  case length(I#invoice.items) - ItemIndex of
    D when D >= 0 -> true;
    D when D < 0 -> false
  end.

select_invoice_items(#invoice{items = undefined}) -> [ ];
select_invoice_items(#invoice{items = Items}) -> Items.

select_invoice_item_at_index({#invoice{items = undefined}, _}) -> not_found;
select_invoice_item_at_index({#invoice{items = [ ]}, _}) -> not_found;
select_invoice_item_at_index({#invoice{items = Items}, Index}) ->
  lists:nth(Index, Items).

replace_item_on_invoice({#invoice{items = Items} = I, Index, InvoiceItem}) ->
  I#invoice{items = splice_list_at(Index, Items, [InvoiceItem])}.

remove_item_from_invoice({#invoice{items = Items} = I, Index}) ->
  I#invoice{items = splice_list_at(Index, Items, [ ])}.

splice_list_at(Index, List, Replacement) ->
  recompose_list(lists:split(Index - 1, List), Replacement).

recompose_list({Before, [_|After]}, Replacement) ->
  lists:flatten([Before, Replacement, After]).

save_new_invoice_item(I) ->
  case handle_update_invoice(I) of
    ok -> length(I#invoice.items);
    AnythingElse -> AnythingElse
  end.

append_item_to_invoice({#invoice{items = undefined} = I, Item}) ->
  I#invoice{items = [Item]};

append_item_to_invoice({#invoice{items = Items} = I, Item}) ->
  I#invoice{items = Items ++ [Item]}.

compute_all_item_prices(I = #invoice{ }) ->
  I#invoice{items = compute_all_item_prices(I#invoice.items)};

compute_all_item_prices(ItemList) when is_list(ItemList) ->
  [compute_single_item_price(I) || I <- ItemList];

compute_all_item_prices(undefined) -> undefined.

compute_single_item_price(#invoice_item{ quantity = Q, unit_price = P } = I) ->
  I#invoice_item{ total_price = round(Q * P, 2) }.

compute_invoice_price(I = #invoice{ }) ->
  I#invoice{ total_price = add_all_item_prices(I#invoice.items, 0) }.

add_all_item_prices(undefined, _) -> undefined;
add_all_item_prices([ ], TotalPrice) -> TotalPrice;
add_all_item_prices([Head|Tail], TotalPrice) ->
  add_all_item_prices(Tail, TotalPrice + Head#invoice_item.total_price).

switch_invoice_to_submitted({I = #invoice{ }, PONumber}) ->
  I#invoice{
    invoice_number = invoices_crypto:make_invoice_number(),
    po_number = PONumber,
    status = submitted
  }.

switch_invoice_to_created(I = #invoice{ }) ->
  I#invoice{status = created}.

switch_invoice_to_cancelled(I = #invoice{ }) ->
  I#invoice{status = cancelled}.

remove_invoice_items(I = #invoice{ }) ->
  I#invoice{items = [ ], total_price = undefined}.

clear_invoice_identifiers(I = #invoice{ }) ->
  I#invoice{
    po_number = undefined,
    invoice_number = undefined,
    issue_date = undefined
  }.

round(Number, Precision) ->
  Power = math:pow(10, Precision),
  round(Number * Power) / Power.
