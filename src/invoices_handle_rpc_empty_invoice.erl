%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2021 2:07 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_rpc_empty_invoice).
-author("Leonardo R. Teixeira").

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).

-define(INVOICE_NOT_FOUND_MSG, "The invoice with id ~p was not found").

-define(INTERNAL_ERROR_BODY, <<"{\"message\":\"Internal Server Error\"}">>).

-define(HEADERS, #{
  <<"content-type">> => <<"application/json">>
}).

-define(INVOICE_CANCELLED, {
  conflict, {"Invalid invoice state.", "invoice-cancelled"}
}).

-define(INVOICE_SUBMITTED, {
  conflict, {"Invalid invoice state.", "invoice-submitted"}
}).

-define(MISSING_INVOICE_ID, {
  bad_request, {"Missing argument.", "missing-invoice-id"}
}).

-define(MISSING_ARGUMENTS, {
  bad_request, {"Malformed payload.", "missing-arguments-map"}
}).

get_paths() ->
  [ {"/behaviour/empty-invoice", ?MODULE, [ ]} ].

init(Request = #{method := <<"OPTIONS">>}, State) ->
  chain_apply({no_content, Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun assemble_response_payload/1
  ]);

init(Request, State) ->
  chain_apply({Request, State}, [
    fun parse_rpc_payload/1,
    fun run_business_logic/1,
    fun log_internal_errors/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun assemble_response_payload/1
  ]).

run_business_logic(Arguments) ->
  chain_apply(Arguments, [
    fun unwrap_arguments/1,
    fun get_invoice_id/1,
    fun try_emptying_invoice/1
  ]).

parse_rpc_payload({Request, State}) ->
  decode_payload(cowboy_req:read_body(Request), State).

decode_payload({ok, Payload, Request}, State) ->
  {jsx:decode(Payload), Request, State}.

unwrap_arguments({JsonMap, Request, State}) ->
  case maps:get(<<"arguments">>, JsonMap, undefined) of
    undefined -> {break, {?MISSING_ARGUMENTS, Request, State}};
    Arguments -> {Arguments, Request, State}
  end.

get_invoice_id({Arguments, Request, State}) ->
  case maps:get(<<"invoiceId">>, Arguments, undefined) of
    undefined -> {break, {?MISSING_INVOICE_ID, Request, State}};
    InvoiceId -> {InvoiceId, Request, State}
  end.

try_emptying_invoice({InvoiceId, Request, State}) ->
  case invoices_use_cases:empty_invoice(InvoiceId) of
    ok -> {no_content, Request, State};
    not_found -> {{not_found, explain_id_not_found(InvoiceId)}, Request, State};
    {error, invoice_submitted} -> {?INVOICE_SUBMITTED, Request, State};
    {error, invoice_cancelled} -> {?INVOICE_CANCELLED, Request, State};
    {error, Reason} -> {{internal_error, Reason}, Request, State}
  end.

explain_id_not_found(InvoiceId) ->
  {io_lib:format(?INVOICE_NOT_FOUND_MSG, [InvoiceId]), "not-found"}.

log_internal_errors({{internal_error, Reason} = Payload, Request, State}) ->
  lager:error(Reason), {Payload, Request, State};

log_internal_errors({Payload, Request, State}) ->
  {Payload, Request, State}.

assemble_response_payload({no_content, Request, State}) ->
  {ok, cowboy_req:reply(204, #{ }, <<>>, Request), State};

assemble_response_payload({{bad_request, BodyData}, Request, State}) ->
  {ok, cowboy_req:reply(400, ?HEADERS, make_body(BodyData), Request), State};

assemble_response_payload({{not_found, BodyData}, Request, State}) ->
  {ok, cowboy_req:reply(404, ?HEADERS, make_body(BodyData), Request), State};

assemble_response_payload({{conflict, BodyData}, Request, State}) ->
  {ok, cowboy_req:reply(409, ?HEADERS, make_body(BodyData), Request), State};

assemble_response_payload({{internal_error, _}, Request, State}) ->
  {ok, cowboy_req:reply(500, ?HEADERS, ?INTERNAL_ERROR_BODY, Request), State}.

make_body({Message, Token}) ->
  jsx:encode(#{
    <<"message">> => list_to_binary(Message),
    <<"token">> => list_to_binary(Token)
  }).
