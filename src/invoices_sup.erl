%%%-------------------------------------------------------------------
%% @doc invoices top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(invoices_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init([]) ->
    SupFlags = #{strategy => one_for_all,
                 intensity => 0,
                 period => 1},

    InvoicesDatabaseChild = #{
      id => invoices_database,
      start => {invoices_database, start_link, [ ]},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [invoices_database]
    },

    InvoiceRestMapperChild = #{
      id => invoices_invoice_rest_mapper,
      start => {invoices_invoice_rest_mapper, start_link, [ ]},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [invoices_invoice_rest_mapper]
    },

    InvoiceItemRestMapperChild = #{
      id => invoices_invoice_item_rest_mapper,
      start => {invoices_invoice_item_rest_mapper, start_link, [ ]},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [invoices_invoice_item_rest_mapper]
    },

    UseCasesChild = #{
      id => invoices_use_cases,
      start => {invoices_use_cases, start_link, [ ]},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [invoices_use_cases]
    },

    ChildSpecs = [
      InvoicesDatabaseChild,
      InvoiceRestMapperChild,
      InvoiceItemRestMapperChild,
      UseCasesChild
    ],

    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
