%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.org
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(invoices_database).

-behaviour(gen_server).

-export([start_link/0, recreate/0, dump_to_text_file/0, load_from_text_file/1,
  find_all_invoices/0, find_invoice_by_id/1, invoice_exists_with_id/1,
  insert_invoice/1, update_invoice/1, delete_invoice/1, generate_next/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(gen_state, {}).

-record(generator, { id, value }).

-include("invoices_records.hrl").

recreate() ->
  gen_server:call(?SERVER, recreate).

dump_to_text_file() ->
  gen_server:call(?SERVER, dump_to_text_file).

load_from_text_file(Path) ->
  gen_server:call(?SERVER, {load_from_text_file, Path}).

generate_next(Generator) ->
  gen_server:call(?SERVER, {generate_next, Generator}).

find_all_invoices() ->
  gen_server:call(?SERVER, find_all_invoices).

find_invoice_by_id(Id) ->
  gen_server:call(?SERVER, {find_invoice_by_id, Id}).

invoice_exists_with_id(Id) ->
  gen_server:call(?SERVER, {invoice_exists_with_id, Id}).

insert_invoice(I = #invoice{ }) ->
  gen_server:call(?SERVER, {insert_invoice, I}).

update_invoice(I = #invoice{ }) ->
  gen_server:call(?SERVER, {update_invoice, I}).

delete_invoice(Id) ->
  gen_server:call(?SERVER, {delete_invoice, Id}).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  application:set_env(mnesia, dir, invoices_paths:get_mnesia_path()),
  application:start(mnesia),
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init([]) ->
  {ok, #gen_state{}}.

handle_call({insert_invoice, I = #invoice{ }}, _From, State = #gen_state{ }) ->
  {reply, handle_insert_invoice(I), State};

handle_call({update_invoice, I = #invoice{ }}, _From, State = #gen_state{ }) ->
  {reply, handle_update_invoice(I), State};

handle_call({delete_invoice, Id}, _From, State = #gen_state{ }) ->
  {reply, handle_delete_invoice(Id), State};

handle_call({invoice_exists_with_id, Id}, _From, State = #gen_state{ }) ->
  {reply, handle_invoice_exists_with_id(Id), State};

handle_call({find_invoice_by_id, Id}, _From, State = #gen_state{ }) ->
  {reply, handle_find_invoice_by_id(Id), State};

handle_call(find_all_invoices, _From, State = #gen_state{ }) ->
  T = fun() -> mnesia:select(invoice, [{'_', [ ], ['$_']}]) end,
  {atomic, Reply} = mnesia:transaction(T),
  {reply, Reply, State};

handle_call(recreate, _From, State = #gen_state{ }) ->
  {reply, handle_recreate(), State};

handle_call(dump_to_text_file, _From, State = #gen_state{ }) ->
  ensure_mnesia_export_path_exists(),
  ok = mnesia:dump_to_textfile(invoices_paths:get_mnesia_export_file_path()),
  {reply, ok, State};

handle_call({load_from_text_file, Path}, _From, State = #gen_state{ }) ->
  {atomic, ok} = mnesia:load_textfile(Path),
  {reply, ok, State};

handle_call({generate_next, Generator}, _From, State = #gen_state{ })
  when is_atom(Generator) ->
  {reply, mnesia:dirty_update_counter(generator, Generator, 1), State};

handle_call(_Request, _From, State = #gen_state{}) ->
  {reply, ok, State}.

handle_cast(_Request, State = #gen_state{}) ->
  {noreply, State}.

handle_info(_Info, State = #gen_state{}) ->
  {noreply, State}.

terminate(_Reason, _State = #gen_state{}) ->
  ok.

code_change(_OldVsn, State = #gen_state{}, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_insert_invoice(I = #invoice{ }) ->
  NextId = mnesia:dirty_update_counter(generator, invoice, 1),
  mnesia:transaction(fun() -> mnesia:write(I#invoice{ id = NextId }) end),
  NextId.

handle_update_invoice(I = #invoice{ }) ->
  FindInvoice = fun() -> mnesia:read(invoice, I#invoice.id, write) end,
  WriteInvoice = fun() -> mnesia:write(I) end,
  case mnesia:transaction(FindInvoice) of
    {atomic, [_]} -> mnesia:transaction(WriteInvoice), ok;
    {atomic, [ ]} -> ok
  end.

handle_delete_invoice(Id) ->
  {atomic, ok} = mnesia:transaction(fun() -> mnesia:delete({invoice, Id}) end),
  ok.

handle_invoice_exists_with_id(Id) ->
  case mnesia:transaction(fun() -> mnesia:read(invoice, Id, write) end) of
    {atomic, [_]} -> true;
    {atomic, [ ]} -> false
  end.

handle_find_invoice_by_id(Id) ->
  case mnesia:transaction(fun() -> mnesia:read(invoice, Id, write) end) of
    {atomic, [Invoice]} -> Invoice;
    {atomic, [ ]} -> not_found
  end.

handle_recreate() ->
  ensure_mnesia_path_exists(),
  application:stop(mnesia),
  ok = try_recreating_schema(),
  application:start(mnesia),
  create_tables(),
  mnesia:info(),
  ok.

try_recreating_schema() ->
  case mnesia:create_schema([node()]) of
    { error, { _, { already_exists, _ } } } ->
      ok = mnesia:delete_schema([node()]),
      try_recreating_schema();
    AnythingElse ->
      AnythingElse
  end.

create_tables() ->
  { atomic, ok } = mnesia:create_table(generator, [
    { disc_copies, [node()]},
    { attributes, record_info(fields, generator) }
  ]),
  { atomic, ok } = mnesia:create_table(invoice, [
    { type, ordered_set },
    { disc_copies, [node()] },
    { attributes, record_info(fields, invoice) }
  ]).

ensure_mnesia_path_exists() ->
  filelib:ensure_dir(invoices_paths:get_mnesia_path()).

ensure_mnesia_export_path_exists() ->
  filelib:ensure_dir(invoices_paths:get_mnesia_export_file_path()).
