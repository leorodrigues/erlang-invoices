%%%-------------------------------------------------------------------
%%% @author leonardo
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Aug 2021 23:52 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_api_specs).
-author("leonardo").

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).
-import(invoices_env, [get_http_port/0]).

get_paths() ->
  [ {"/api-specs", ?MODULE, [ ]} ].

init(Request, State) ->
  chain_apply({Request, State}, [
    fun load_swagger/1,
    fun load_info_section/1,
    fun apply_info_section/1,
    fun load_servers_section/1,
    fun apply_servers_section/1,
    fun encode_swagger/1,
    fun respond_with_success/1
  ]).

load_swagger(State) ->
  {jsx:decode(load_content(invoices_paths:get_swagger_file_path())), State}.

load_info_section({Swagger, State}) ->
  {get_info_section(), Swagger, State}.

apply_info_section({Info, Swagger, State}) ->
  {update_info_section(Info, Swagger), State}.

load_servers_section({Swagger, State}) ->
  {make_servers_section(list_servers()), Swagger, State}.

apply_servers_section({Servers, Swagger, State}) ->
  {update_servers_section(Servers, Swagger), State}.

encode_swagger({Swagger, State}) ->
  {jsx:encode(Swagger), State}.

respond_with_success({BinarySwagger, {Request, State}}) ->
  {ok, cowboy_req:reply(200, get_headers(), BinarySwagger, Request), State}.

list_servers() ->
  case catch net:getifaddrs(#{family => inet, flags => [running]}) of
    {'EXIT', {notsup, _}} -> [ ];
    {ok, AddressList} -> [make_url(Address) || Address <- AddressList];
    Result -> erlang:error(unexpected_address_list_format, [Result])
  end.

make_url(Address) ->
  io_lib:format("http://~s:~p", [get_address_field(Address), get_http_port()]).

get_address_field(Address) ->
  as_ip_string(maps:get(addr, maps:get(addr, Address))).

as_ip_string({A, B, C, D}) ->
  io_lib:format("~w.~w.~w.~w", [A, B, C, D]).

get_info_section() ->
  case lists:keyfind(invoices_app, 1, application:loaded_applications()) of
    {_, Description, Version} -> make_info_section(Description, Version);
    Result -> erlang:error(unexpected_app_spec, [Result])
  end.

update_info_section(NewInfo, Swagger) ->
  case maps:get(<<"info">>, Swagger, undefined) of
    undefined -> maps:put(<<"info">>, NewInfo, Swagger);
    Info -> maps:put(<<"info">>, maps:merge(Info, NewInfo), Swagger)
  end.

update_servers_section(NewServers, Swagger) ->
  case maps:get(<<"servers">>, Swagger, undefined) of
    undefined -> maps:put(<<"servers">>, NewServers, Swagger);
    Servers -> maps:put(<<"servers">>, Servers ++ NewServers, Swagger)
  end.

make_info_section(AppDescription, AppVersion) ->
  #{
    <<"description">> => list_to_binary(AppDescription),
    <<"version">> => list_to_binary(AppVersion)
  }.

make_servers_section(ServerList) ->
  [#{<<"url">> => list_to_binary(U)} || U <- ServerList].

load_content(Path) -> {ok, C} = file:read_file(Path), C.

get_headers() ->
  #{
    <<"content-type">> => <<"application/json">>,
    <<"cache-control">> => <<"no-cache">>
  }.