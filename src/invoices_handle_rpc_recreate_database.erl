%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 12. Oct 2021 2:20 PM
%%%-------------------------------------------------------------------
-module(invoices_handle_rpc_recreate_database).
-author("Leonardo R. Teixeira").

%% API

-export([get_paths/0, init/2]).

-import(church, [chain_apply/2]).

-define(DEFAULT_DELAY, 3000).

get_paths() ->
  [ {"/behaviour/recreate-database", ?MODULE, [ ]} ].

init(Request = #{method := <<"OPTIONS">>}, State) ->
  chain_apply({Request, State}, [
    fun invoices_handle_response_headers:set_options_headers/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun respond_with_no_content/1
  ]);

init(Request, State) ->
  chain_apply({Request, State}, [
    fun fire_database_recreation/1,
    fun invoices_handle_response_headers:set_response_headers/1,
    fun respond_with_accepted/1
  ]).

fire_database_recreation(State) ->
  spawn(fun() -> invoices_database:recreate() end),
  State.

respond_with_accepted({HttpRequest, State}) ->
  {ok, cowboy_req:reply(202, #{ }, <<>>, HttpRequest), State}.

respond_with_no_content({HttpRequest, State}) ->
  {ok, cowboy_req:reply(204, #{ }, <<>>, HttpRequest), State}.
