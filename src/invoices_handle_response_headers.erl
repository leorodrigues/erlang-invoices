%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 12. Oct 2021 22:44
%%%-------------------------------------------------------------------
-module(invoices_handle_response_headers).
-author("Leonardo R. Teixeira").

-import(church, [chain_apply/2]).

-export([set_response_headers/1, set_options_headers/1]).

-define(HEADER_ACL_METHODS, <<"access-control-allow-methods">>).
-define(HEADER_ACL_ORIGIN, <<"access-control-allow-origin">>).
-define(HEADER_ACL_HEADERS, <<"access-control-allow-headers">>).
-define(VALUE_ACL_METHODS, <<"GET, POST, OPTIONS">>).
-define(VALUE_ACL_HEADERS, <<"content-type">>).
-define(VALUE_ACL_ORIGIN, <<"*">>).

set_response_headers({Payload, Request, State}) ->
  {Payload, set_allow_origin(Request), State};

set_response_headers({Request, State}) ->
  {set_allow_origin(Request), State}.

set_options_headers({Payload, Request, State}) ->
  {Payload, add_options_headers(Request), State};

set_options_headers({Request, State}) ->
  {add_options_headers(Request), State}.

add_options_headers(Request) ->
  chain_apply(Request, [
    fun set_acl_headers_allowed/1,
    fun set_acl_methods_allowed/1
  ]).

set_allow_origin(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_ORIGIN, ?VALUE_ACL_ORIGIN, Request).

set_acl_methods_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_METHODS, ?VALUE_ACL_METHODS, Request).

set_acl_headers_allowed(Request) ->
  cowboy_req:set_resp_header(?HEADER_ACL_HEADERS, ?VALUE_ACL_HEADERS, Request).
