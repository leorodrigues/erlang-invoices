%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 18. Sep 2021 8:24 PM
%%%-------------------------------------------------------------------
-author("Leonardo R. Teixeira").

-type day() :: 1..31.
-type month() :: 1..12.
-type year() :: pos_integer().
-type hour() :: 0..23.
-type minute() :: 0..59.
-type second() :: 0..59.
-type date() :: {year(), month(), day()}.
-type time() :: {hour(), minute(), second()}.
-type date_time() :: {date(), time()}.

-record(contact, {
  name :: string(),
  email :: string()
}).

-record(company, {
  trading_name :: string(),
  company_name :: string(),
  fiscal_number :: string(),
  complete_address :: string(),
  contact :: #contact{ }
}).

-record(bank_info, {
  complete_address :: string(),
  swift_number :: string(),
  iban_number :: string(),
  account_holder_name :: string(),
  account_number :: string(),
  agency_number :: string()
}).

-record(remittance_info, {
  intermediary_bank :: #bank_info{ },
  final_bank :: #bank_info{ }
}).

-record(invoice_item, {
  quantity :: pos_integer(),
  description :: string(),
  unit_price :: float(),
  total_price :: float()
}).

-record(invoice, {
  id :: pos_integer(),
  status :: undefined | created | submitted | cancelled,
  invoice_number :: string(),
  po_number :: string(),
  provider :: #company{ },
  customer :: #company{ },
  issue_date :: date_time(),
  due_date :: date_time(),
  payment_method :: string(),
  remittance_info :: #remittance_info{ },
  items :: [#invoice_item{ }],
  total_price :: float(),
  description :: string()
}).
