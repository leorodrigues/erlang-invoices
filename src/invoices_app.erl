%%%-------------------------------------------------------------------
%% @doc invoices public API
%% @end
%%%-------------------------------------------------------------------

-module(invoices_app).

-behaviour(application).

-import(lists, [foldl/3]).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
  Dispatch = cowboy_router:compile([
    {'_', collect_paths([
      invoices_handle_api_docs,
      invoices_handle_api_specs,
      invoices_handle_invoice_collection,
      invoices_handle_single_invoice,
      invoices_handle_invoice_item_collection,
      invoices_handle_single_invoice_item,
      invoices_handle_rpc_empty_invoice,
      invoices_handle_rpc_submit_invoice,
      invoices_handle_rpc_cancel_invoice,
      invoices_handle_rpc_duplicate_invoice,
      invoices_handle_rpc_recreate_database,
      invoices_handle_rpc_shutdown
    ])}
  ]),
  {ok, _} = cowboy:start_clear(invoices_http, [{port, 8080}], #{
    env => #{dispatch => Dispatch}
  }),
  lager:info("invoice service is running"),
  invoices_sup:start_link().

stop(_State) ->
  cowboy:stop_listener(invoices_http),
  ok.

%% internal functions

collect_paths(Handlers) ->
  Finner = fun(P, Paths) -> [P|Paths] end,
  FOuter = fun(H, Paths) -> foldl(Finner, Paths, apply(H, get_paths, [ ])) end,
  foldl(FOuter, [ ], Handlers).