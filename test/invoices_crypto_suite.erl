%%%-------------------------------------------------------------------
%%% @author lrteixeira
%%% @copyright (C) 2021, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Oct 2021 06:12
%%%-------------------------------------------------------------------
-module(invoices_crypto_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").
-include("../src/invoices_records.hrl").

-export([all/0, init_per_testcase/2, end_per_testcase/2,
  init_per_suite/1, end_per_suite/1]).

-export([make_invoice_number_1/1, make_invoice_number_2/1]).

make_invoice_number_1(_) ->
  ct:comment("Should output the next invoice number"),
  meck:expect(invoices_database, generate_next, fun(_) -> 42 end),
  <<"000042">> = invoices_crypto:make_invoice_number(),
  1 = meck:num_calls(invoices_database, generate_next, [invoice_number]).

make_invoice_number_2(_) ->
  ct:comment("Should output any db errors while computing the next invoice number"),
  meck:expect(invoices_database, generate_next, fun(_) -> {error, fake} end),
  {error, fake} = invoices_crypto:make_invoice_number(),
  1 = meck:num_calls(invoices_database, generate_next, [invoice_number]).

all() -> [make_invoice_number_1, make_invoice_number_2].

init_per_testcase(_Case, Config) ->
  meck:new(invoices_database, [stub_all]),
  Config.

end_per_testcase(_Case, Config) ->
  meck:unload(invoices_database),
  Config.

init_per_suite(Config) ->
%%  load_suite_data(Config).
  Config.

end_per_suite(_Config) ->
  ok.
