%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.org
%%% @doc
%%%
%%% @end
%%% Created : 25. Sep 2021 6:05 PM
%%%-------------------------------------------------------------------
-module(invoices_use_cases_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").
-include("../src/invoices_records.hrl").

-define(SERVER_NAME, invoices_use_cases).

-export([all/0, init_per_testcase/2, end_per_testcase/2,
  init_per_suite/1, end_per_suite/1]).

-export([
  find_all_invoices_1/1, find_invoice_by_id_1/1,

  invoice_exists_with_id_1/1,

  create_invoice_1/1, create_invoice_2/1, create_invoice_3/1,

  update_invoice_1/1, update_invoice_2/1, update_invoice_3/1,
  update_invoice_4/1, update_invoice_5/1, update_invoice_6/1,
  update_invoice_7/1, update_invoice_8/1,

  delete_invoice_1/1, delete_invoice_2/1, delete_invoice_3/1,

  find_all_invoice_items_1/1, find_all_invoice_items_2/1,
  find_all_invoice_items_3/1, find_all_invoice_items_4/1,

  create_invoice_item_1/1, create_invoice_item_2/1, create_invoice_item_3/1,
  create_invoice_item_4/1, create_invoice_item_5/1, create_invoice_item_6/1,
  create_invoice_item_7/1,

  update_invoice_item_1/1, update_invoice_item_2/1, update_invoice_item_3/1,
  update_invoice_item_4/1, update_invoice_item_5/1, update_invoice_item_6/1,
  update_invoice_item_7/1, update_invoice_item_8/1,

  delete_invoice_item_1/1, delete_invoice_item_2/1, delete_invoice_item_3/1,
  delete_invoice_item_4/1, delete_invoice_item_5/1, delete_invoice_item_6/1,
  delete_invoice_item_7/1, delete_invoice_item_8/1, delete_invoice_item_9/1,

  find_invoice_item_by_index_1/1, find_invoice_item_by_index_2/1,
  find_invoice_item_by_index_3/1, find_invoice_item_by_index_4/1,
  find_invoice_item_by_index_5/1,

  submit_invoice_1/1, submit_invoice_2/1, submit_invoice_3/1,
  submit_invoice_4/1, submit_invoice_5/1,

  cancel_invoice_1/1, cancel_invoice_2/1, cancel_invoice_3/1,
  cancel_invoice_4/1, cancel_invoice_5/1,

  duplicate_invoice_1/1, duplicate_invoice_2/1, duplicate_invoice_3/1,
  duplicate_invoice_4/1,

  empty_invoice_1/1, empty_invoice_2/1, empty_invoice_3/1, empty_invoice_4/1,
  empty_invoice_5/1,

  invoice_item_exists_at_1/1, invoice_item_exists_at_2/1,
  invoice_item_exists_at_3/1, invoice_item_exists_at_4/1,
  invoice_item_exists_at_5/1
]).

-import(test_support, [load_case_data/3, load_suite_data/1]).

-define(ALREADY_SUBMITTED, {error, invoice_already_submitted}).
-define(CANCELLED, {error, invoice_cancelled}).

find_all_invoices_1(_) ->
  ct:comment("Should delegate the call to the database."),
  meck:expect(invoices_database, find_all_invoices, fun() -> [ ] end),
  [ ] = invoices_use_cases:find_all_invoices(),
  1 = meck:num_calls(invoices_database, find_all_invoices, []).

find_invoice_by_id_1(Config) ->
  ct:comment("Should delegate to the database."),
  Invoice = load_case_data(Config, find_invoice_by_id_1, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  Invoice = invoices_use_cases:find_invoice_by_id(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

invoice_exists_with_id_1(_) ->
  ct:comment("Should delegate to the database."),
  meck:expect(invoices_database, invoice_exists_with_id, fun(_) -> true end),
  true = invoices_use_cases:invoice_exists_with_id(112358),
  1 = meck:num_calls(invoices_database, invoice_exists_with_id, [112358]).

create_invoice_1(Config) ->
  ct:comment("Should save the new invoice in the database."),
  MinimalInvoice = load_case_data(Config, create_invoice_1, [minimal]),
  PersistedInvoice = load_case_data(Config, create_invoice_1, [persisted]),
  meck:expect(invoices_database, insert_invoice, fun(_) -> 112358 end),
  112358 = invoices_use_cases:create_invoice(MinimalInvoice),
  1 = meck:num_calls(invoices_database, insert_invoice, [PersistedInvoice]).

create_invoice_2(Config) ->
  ct:comment("Should compute the total price of the invoice during creation."),
  MinimalInvoice = load_case_data(Config, create_invoice_2, [minimal_invoice]),
  PricedInvoice = load_case_data(Config, create_invoice_2, [priced_invoice]),
  meck:expect(invoices_database, insert_invoice, fun(_) -> 112358 end),
  112358 = invoices_use_cases:create_invoice(MinimalInvoice),
  1 = meck:num_calls(invoices_database, insert_invoice, [PricedInvoice]).

create_invoice_3(_) ->
  ct:comment("Should persist an empty invoice without any problems."),
  OriginalInvoice = #invoice{ },
  PersistedInvoice = #invoice{status = created},
  meck:expect(invoices_database, insert_invoice, fun(_) -> 132134 end),
  132134 = invoices_use_cases:create_invoice(OriginalInvoice),
  1 = meck:num_calls(invoices_database, insert_invoice, [PersistedInvoice]).

update_invoice_1(Config) ->
  ct:comment("Should update the existing record."),
  MinimalInvoice = load_case_data(Config, update_invoice_1, [minimal_invoice]),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:update_invoice(MinimalInvoice),
  1 = meck:num_calls(invoices_database, update_invoice, [MinimalInvoice]).

update_invoice_2(Config) ->
  ct:comment("Should compute the total price of the invoice during update."),
  PricedInvoice = load_case_data(Config, update_invoice_2, [priced_invoice]),
  MinimalInvoice = load_case_data(Config, update_invoice_2, [minimal_invoice]),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:update_invoice(MinimalInvoice),
  1 = meck:num_calls(invoices_database, update_invoice, [PricedInvoice]).

update_invoice_3(_) ->
  ct:comment("Should be possible to \"nullify\" an invoice via update"),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:update_invoice(#invoice{id = 42}),
  1 = meck:num_calls(invoices_database, update_invoice, [#invoice{id = 42}]).

update_invoice_4(_) ->
  ct:comment("Should be impossible to update an invoice w/o id"),
  {error, _} = invoices_use_cases:update_invoice(#invoice{ }),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_5(_) ->
  ct:comment("Should be impossible to update an invoice with string id"),
  {error, _} = invoices_use_cases:update_invoice(#invoice{id = "abc"}),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_6(_) ->
  ct:comment("Should be impossible to update an invoice with a negative id"),
  {error, _} = invoices_use_cases:update_invoice(#invoice{id = -13}),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_7(Config) ->
  ct:comment("Should be impossible to update an invoice already submitted"),
  Invoice = load_case_data(Config, update_invoice_7, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:update_invoice(Invoice),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_8(Config) ->
  ct:comment("Should be impossible to update an invoice already cancelled"),
  Invoice = load_case_data(Config, update_invoice_8, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:update_invoice(Invoice),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_1(Config) ->
  ct:comment("Should delete existing record."),
  Invoice = load_case_data(Config, delete_invoice_1, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  meck:expect(invoices_database, delete_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:delete_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, delete_invoice, [112358]).

delete_invoice_2(_) ->
  ct:comment("Should be impossible to delete an invoice already cancelled"),
  Invoice = #invoice{status = cancelled},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:delete_invoice(42),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_3(_) ->
  ct:comment("Should be impossible to delete an invoice already submitted"),
  Invoice = #invoice{status = submitted},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:delete_invoice(42),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

find_all_invoice_items_1(Config) ->
  ct:comment("Should be possible to list all items from an invoice."),
  Invoice = load_case_data(Config, find_all_invoice_items_1, [invoice]),
  Items = load_case_data(Config, find_all_invoice_items_1, [items]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  Items = invoices_use_cases:find_all_invoice_items(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_all_invoice_items_2(Config) ->
  ct:comment("Should return an empty list if the invoice item list is empty."),
  Invoice = load_case_data(Config, find_all_invoice_items_2, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  [ ] = invoices_use_cases:find_all_invoice_items(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_all_invoice_items_3(_) ->
  ct:comment("Should inform that the items where not found if the invoice is not found."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:find_all_invoice_items(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_all_invoice_items_4(_) ->
  ct:comment("Should return any database errors while trying to list the items of an invoice."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:find_all_invoice_items(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_invoice_item_by_index_1(Config) ->
  ct:comment("Should be possible to pick a single item from an invoice."),
  Invoice = load_case_data(Config, find_invoice_item_by_index_1, [invoice]),
  Item = load_case_data(Config, find_invoice_item_by_index_1, [item]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  Item = invoices_use_cases:find_invoice_item_by_index(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_invoice_item_by_index_2(Config) ->
  ct:comment("Should return 'not_found' if the invoice's item list is 'undefined'."),
  Invoice = load_case_data(Config, find_invoice_item_by_index_2, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  not_found = invoices_use_cases:find_invoice_item_by_index(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_invoice_item_by_index_3(Config) ->
  ct:comment("Should return 'not_found' if the invoice's item list is empty."),
  Invoice = load_case_data(Config, find_invoice_item_by_index_3, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  not_found = invoices_use_cases:find_invoice_item_by_index(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_invoice_item_by_index_4(_) ->
  ct:comment("Should return 'not_found' if the invoice itself is not found."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:find_invoice_item_by_index(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

find_invoice_item_by_index_5(_) ->
  ct:comment("Should return any errors the database may produce."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:find_invoice_item_by_index(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]).

invoice_item_exists_at_1(_) ->
  ct:comment("Should return true if the invoice exists and also the item."),
  Invoice = #invoice{items = [#invoice_item{ }]},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  true = invoices_use_cases:invoice_item_exists_at(42, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]).

invoice_item_exists_at_2(_) ->
  ct:comment("Should return false if the invoice exists but not the item."),
  Invoice = #invoice{items = [#invoice_item{ }]},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  false = invoices_use_cases:invoice_item_exists_at(42, 2),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]).

invoice_item_exists_at_3(_) ->
  ct:comment("Should return false if the invoice exists but the item list is empty."),
  Invoice = #invoice{items = [ ]},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  false = invoices_use_cases:invoice_item_exists_at(17, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [17]).

invoice_item_exists_at_4(_) ->
  ct:comment("Should return false if the invoice exists but the item list is undefined."),
  Invoice = #invoice{ },
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  false = invoices_use_cases:invoice_item_exists_at(19, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [19]).

invoice_item_exists_at_5(_) ->
  ct:comment("Should return 'not_found' if the invoice does not exist."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:invoice_item_exists_at(17, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [17]).

create_invoice_item_1(Config) ->
  ct:comment("Should add an item to an existing invoice."),
  Item = load_case_data(Config, create_invoice_item_1, [item]),
  Original = load_case_data(Config, create_invoice_item_1, [original]),
  Updated = load_case_data(Config, create_invoice_item_1, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  2 = invoices_use_cases:create_invoice_item(112358, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

create_invoice_item_2(_) ->
  ct:comment("Should return 'not_found' if the invoice doesn't exists during item creation."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:create_invoice_item(112358, #invoice_item{ }),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

create_invoice_item_3(_) ->
  ct:comment("Should return errors while trying to find the invoice before adding the item."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:create_invoice_item(112358, #invoice_item{ }),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

create_invoice_item_4(Config) ->
  ct:comment("Should return errors while trying to persist the invoice with the new item."),
  Item = load_case_data(Config, create_invoice_item_4, [item]),
  OriginalInvoice = load_case_data(Config, create_invoice_item_4, [original]),
  UpdatedInvoice = load_case_data(Config, create_invoice_item_4, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> OriginalInvoice end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:create_invoice_item(112358, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [UpdatedInvoice]).

create_invoice_item_5(Config) ->
  ct:comment("Should be possible to add an item to an undefined item list."),
  Item = load_case_data(Config, create_invoice_item_5, [item]),
  Original = load_case_data(Config, create_invoice_item_5, [original]),
  Updated = load_case_data(Config, create_invoice_item_5, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  1 = invoices_use_cases:create_invoice_item(112358, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

create_invoice_item_6(_) ->
  ct:comment("Should be impossible to create an item for an invoice already cancelled"),
  Invoice = #invoice{status = cancelled}, Item = #invoice_item{ },
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:create_invoice_item(42, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

create_invoice_item_7(_) ->
  ct:comment("Should be impossible to create an item for an invoice already submitted"),
  Invoice = #invoice{status = submitted}, Item = #invoice_item{ },
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:create_invoice_item(42, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_1(Config) ->
  ct:comment("Should substitute the first element of the invoice"),
  Item = load_case_data(Config, update_invoice_item_1, [item]),
  Original = load_case_data(Config, update_invoice_item_1, [original]),
  Updated = load_case_data(Config, update_invoice_item_1, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:update_invoice_item(112358, 1, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

update_invoice_item_2(_) ->
  ct:comment("Should return 'not_found' if the invoice doesn't exists during item update"),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:update_invoice_item(112358, 1, #invoice_item{ }),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_3(_) ->
  ct:comment("Should return errors while trying to find the invoice before replacing the item."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:update_invoice_item(112358, 1, #invoice_item{ }),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_4(Config) ->
  ct:comment("Should return errors while trying to persist the invoice with the replaced item."),
  Item = load_case_data(Config, update_invoice_item_4, [item]),
  OriginalInvoice = load_case_data(Config, update_invoice_item_4, [original]),
  UpdatedInvoice = load_case_data(Config, update_invoice_item_4, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> OriginalInvoice end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:update_invoice_item(112358, 1, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [UpdatedInvoice]).

update_invoice_item_5(Config) ->
  ct:comment("Should do nothing if the item index is beyond the length of the invoice."),
  Item = load_case_data(Config, update_invoice_item_5, [item]),
  OriginalInvoice = load_case_data(Config, update_invoice_item_5, [original]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> OriginalInvoice end),
  ok = invoices_use_cases:update_invoice_item(112358, 3, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_6(Config) ->
  ct:comment("Should skip updating the invoice if the item list is undefined."),
  Item = load_case_data(Config, update_invoice_item_6, [item]),
  OriginalInvoice = load_case_data(Config, update_invoice_item_6, [original]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> OriginalInvoice end),
  ok = invoices_use_cases:update_invoice_item(112358, 3, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_7(_) ->
  ct:comment("Should be impossible to update an item on an invoice already cancelled"),
  Invoice = #invoice{status = cancelled}, Item = #invoice_item{ },
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:update_invoice_item(42, 1, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

update_invoice_item_8(_) ->
  ct:comment("Should be impossible to update an item on an invoice already submitted"),
  Invoice = #invoice{status = submitted}, Item = #invoice_item{ },
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:update_invoice_item(42, 1, Item),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_1(Config) ->
  ct:comment("Should remove the first item from an invoice."),
  Original = load_case_data(Config, delete_invoice_item_1, [original]),
  Updated = load_case_data(Config, delete_invoice_item_1, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:delete_invoice_item(112358, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

delete_invoice_item_2(Config) ->
  ct:comment("Should do nothing if the item index is beyond the end of the list"),
  Invoice = load_case_data(Config, delete_invoice_item_2, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  ok = invoices_use_cases:delete_invoice_item(112358, 3),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_3(_) ->
  ct:comment("Should return 'not_found' if the invoice does not exist while running delete item"),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:delete_invoice_item(112358, 99),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_4(_) ->
  ct:comment("Should return any errors while trying to find an invoice for removing an item"),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:delete_invoice_item(112358, 99),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_5(Config) ->
  ct:comment("Should return any errors while trying to persist an invoice after removing an item"),
  Original = load_case_data(Config, delete_invoice_item_5, [original]),
  Updated = load_case_data(Config, delete_invoice_item_5, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:delete_invoice_item(112358, 2),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

delete_invoice_item_6(Config) ->
  ct:comment("Should remove the last item from an invoice."),
  Original = load_case_data(Config, delete_invoice_item_6, [original]),
  Updated = load_case_data(Config, delete_invoice_item_6, [updated]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:delete_invoice_item(112358, 2),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Updated]).

delete_invoice_item_7(Config) ->
  ct:comment("Should finish removing an item even if the list is undefined."),
  Original = load_case_data(Config, delete_invoice_item_7, [original]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:delete_invoice_item(112358, 2),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_8(_) ->
  ct:comment("Should be impossible to delete an item on an invoice already cancelled"),
  Invoice = #invoice{status = cancelled},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:delete_invoice_item(42, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

delete_invoice_item_9(_) ->
  ct:comment("Should be impossible to delete an item on an invoice already submitted"),
  Invoice = #invoice{status = submitted},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:delete_invoice_item(42, 1),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [42]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

submit_invoice_1(Config) ->
  ct:comment("Should stamp the invoice with a number, po_number and correct status."),
  Original = load_case_data(Config, submit_invoice_1, [original]),
  Submitted = load_case_data(Config, submit_invoice_1, [submitted]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  meck:expect(invoices_crypto, make_invoice_number, fun() -> <<"TESTIVC123">> end),
  ok = invoices_use_cases:submit_invoice(132134, <<"123ABC">>),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [132134]),
  1 = meck:num_calls(invoices_database, update_invoice, [Submitted]),
  1 = meck:num_calls(invoices_crypto, make_invoice_number, [ ]).

submit_invoice_2(Config) ->
  ct:comment("Should be impossible to submit an already submitted invoice."),
  Invoice = load_case_data(Config, submit_invoice_2, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  ?ALREADY_SUBMITTED = invoices_use_cases:submit_invoice(132134, <<"123ABC">>),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [132134]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']),
  0 = meck:num_calls(invoices_crypto, make_invoice_number, [ ]).

submit_invoice_3(Config) ->
  ct:comment("Should be impossible to submit a cancelled invoice."),
  Invoice = load_case_data(Config, submit_invoice_3, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  ?CANCELLED = invoices_use_cases:submit_invoice(132134, <<"123ABC">>),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [132134]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']),
  0 = meck:num_calls(invoices_crypto, make_invoice_number, [ ]).

submit_invoice_4(_) ->
  ct:comment("Should receive any database errors trying to find the invoice for submission."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:submit_invoice(132134, <<"123ABC">>),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [132134]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']),
  0 = meck:num_calls(invoices_crypto, make_invoice_number, [ ]).

submit_invoice_5(Config) ->
  ct:comment("Should receive any database errors trying to save the invoice after submission."),
  Invoice = load_case_data(Config, submit_invoice_5, [invoice]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:submit_invoice(132134, <<"123ABC">>),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [132134]),
  1 = meck:num_calls(invoices_database, update_invoice, ['_']),
  1 = meck:num_calls(invoices_crypto, make_invoice_number, [ ]).

cancel_invoice_1(Config) ->
  ct:comment("Should flip the status of an invoice to 'cancelled'."),
  Original = load_case_data(Config, cancel_invoice_1, [original]),
  Cancelled = load_case_data(Config, cancel_invoice_1, [cancelled]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:cancel_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Cancelled]).

cancel_invoice_2(Config) ->
  ct:comment("Should do nothing if the invoice is already cancelled."),
  Cancelled = load_case_data(Config, cancel_invoice_2, [cancelled]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Cancelled end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:cancel_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Cancelled]).

cancel_invoice_3(_) ->
  ct:comment("Should return 'not_found' if the invoice to be cancelled does not exist."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:cancel_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

cancel_invoice_4(Config) ->
  ct:comment("Should return any database errors while updating the invoice after canelling it."),
  Original = load_case_data(Config, cancel_invoice_4, [original]),
  Cancelled = load_case_data(Config, cancel_invoice_4, [cancelled]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:cancel_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Cancelled]).

cancel_invoice_5(Config) ->
  ct:comment("Should return any database errors while updating the invoice after canelling it."),
  Original = load_case_data(Config, cancel_invoice_5, [original]),
  Cancelled = load_case_data(Config, cancel_invoice_5, [cancelled]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:cancel_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Cancelled]).

duplicate_invoice_1(Config) ->
  ct:comment("Should load the given invoice instance and save it as 'created'."),
  Original = load_case_data(Config, duplicate_invoice_1, [original]),
  Copy = load_case_data(Config, duplicate_invoice_1, [copy]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, insert_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:duplicate_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, insert_invoice, [Copy]).

duplicate_invoice_2(_) ->
  ct:comment("Should return 'not_found' if the invoice to copied does not exist."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> not_found end),
  not_found = invoices_use_cases:duplicate_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, insert_invoice, ['_']).

duplicate_invoice_3(_) ->
  ct:comment("Should return any database errors while loading an invoice to copy it."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:duplicate_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, insert_invoice, ['_']).

duplicate_invoice_4(Config) ->
  ct:comment("Should return any database errors while saving a new invoice after copy."),
  Original = load_case_data(Config, duplicate_invoice_4, [original]),
  Copy = load_case_data(Config, duplicate_invoice_4, [copy]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, insert_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:duplicate_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, insert_invoice, [Copy]).

empty_invoice_1(Config) ->
  ct:comment("Should remove all items and total value from a given invoice."),
  Original = load_case_data(Config, empty_invoice_5, [original]),
  Empty = load_case_data(Config, empty_invoice_5, [empty]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> ok end),
  ok = invoices_use_cases:empty_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Empty]).

empty_invoice_2(_) ->
  ct:comment("Should refuse emptying an invoice already submitted."),
  Invoice = #invoice{status = submitted},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_submitted} = invoices_use_cases:empty_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

empty_invoice_3(_) ->
  ct:comment("Should refuse emptying an invoice already cancelled."),
  Invoice = #invoice{status = cancelled},
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Invoice end),
  {error, invoice_cancelled} = invoices_use_cases:empty_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

empty_invoice_4(_) ->
  ct:comment("Should return any database errors finding the invoice before emptying it."),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:empty_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  0 = meck:num_calls(invoices_database, update_invoice, ['_']).

empty_invoice_5(Config) ->
  ct:comment("Should return any database errors saving the invoice after emptying it."),
  Original = load_case_data(Config, empty_invoice_5, [original]),
  Empty = load_case_data(Config, empty_invoice_5, [empty]),
  meck:expect(invoices_database, find_invoice_by_id, fun(_) -> Original end),
  meck:expect(invoices_database, update_invoice, fun(_) -> {error, fake} end),
  {error, fake} = invoices_use_cases:empty_invoice(112358),
  1 = meck:num_calls(invoices_database, find_invoice_by_id, [112358]),
  1 = meck:num_calls(invoices_database, update_invoice, [Empty]).

%% --- Support functions --- --- --- --- --- --- --- --- --- --- --- --- --- ---

%% --- Common test API functions --- --- --- --- --- --- --- --- --- --- --- ---

all() -> [
  find_all_invoices_1, find_invoice_by_id_1,

  invoice_exists_with_id_1,

  create_invoice_1, create_invoice_2, create_invoice_3,

  update_invoice_1, update_invoice_2, update_invoice_3,
  update_invoice_4, update_invoice_5, update_invoice_6,
  update_invoice_7, update_invoice_8,

  delete_invoice_1, delete_invoice_2, delete_invoice_3,

  find_all_invoice_items_1, find_all_invoice_items_2,
  find_all_invoice_items_3, find_all_invoice_items_4,

  create_invoice_item_1, create_invoice_item_2, create_invoice_item_3,
  create_invoice_item_4, create_invoice_item_5, create_invoice_item_6,
  create_invoice_item_7,

  update_invoice_item_1, update_invoice_item_2, update_invoice_item_3,
  update_invoice_item_4, update_invoice_item_5, update_invoice_item_6,
  update_invoice_item_7, update_invoice_item_8,

  delete_invoice_item_1, delete_invoice_item_2, delete_invoice_item_3,
  delete_invoice_item_4, delete_invoice_item_5, delete_invoice_item_6,
  delete_invoice_item_7, delete_invoice_item_8, delete_invoice_item_9,

  find_invoice_item_by_index_1, find_invoice_item_by_index_2,
  find_invoice_item_by_index_3, find_invoice_item_by_index_4,
  find_invoice_item_by_index_5,

  submit_invoice_1, submit_invoice_2, submit_invoice_3,
  submit_invoice_4, submit_invoice_5,

  cancel_invoice_1, cancel_invoice_2, cancel_invoice_3,
  cancel_invoice_4, cancel_invoice_5,

  duplicate_invoice_1, duplicate_invoice_2, duplicate_invoice_3,
  duplicate_invoice_4,

  empty_invoice_1, empty_invoice_2, empty_invoice_3, empty_invoice_4,
  empty_invoice_5,

  invoice_item_exists_at_1, invoice_item_exists_at_2,
  invoice_item_exists_at_3, invoice_item_exists_at_4,
  invoice_item_exists_at_5
].

init_per_testcase(_Case, Config) ->
  meck:new(invoices_crypto, [stub_all]),
  meck:new(invoices_database, [stub_all]),
  gen_server:start({local, ?SERVER_NAME}, ?SERVER_NAME, [ ], [ ]),
  Config.

end_per_testcase(_Case, Config) ->
  gen_server:stop(?SERVER_NAME, normal, 1000),
  meck:unload(invoices_database),
  meck:unload(invoices_crypto),
  Config.

init_per_suite(Config) ->
  load_suite_data(Config).

end_per_suite(_Config) ->
  ok.
