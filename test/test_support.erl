%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 30. Aug 2021 8:38 AM
%%%-------------------------------------------------------------------
-module(test_support).
-include_lib("common_test/include/ct.hrl").

-author("Leonardo R. Teixeira").

-export([load_suite_data/1, get_data/2, load_case_data/3,
  load_binary_case_data/2]).

load_binary_case_data(Config, Case) ->
  case file:read_file(get_case_file_path(Config, make_file_name(Case))) of
    {ok, Binary} -> Binary;
    {error, _} = E -> E
  end.

load_suite_data(Config) ->
  [{samples, load_terms(get_case_data_path(Config))} | Config].

load_case_data(Config, Case, Path) ->
  get_data(?config(samples, Config), [Case|Path]).

get_data(Config, [ ]) -> Config;
get_data(Config, [Head|Tail]) ->
  case proplists:get_value(Head, Config, undefined) of
    undefined -> erlang:error(path_element_not_found, [Head]);
    AnythingElse -> get_data(AnythingElse, Tail)
  end.

get_case_data_path(Config) ->
  BasePath = ?config(data_dir, Config),
  {BasePath, filename:join(BasePath, "case_data.terms")}.

load_terms({BasePath, FilePath}) ->
  {ok, Terms} = file:consult(FilePath),
  [load_externals(BasePath, T) || T <- Terms].

make_file_name(Case) ->
  io_lib:format("~s.json", [Case]).

get_case_file_path(Config, FileName) ->
  filename:join(?config(data_dir, Config), FileName).

load_externals(BasePath, {external_term, RelativePath}) ->
  {ok, [Term|_]} = file:consult(filename:join(BasePath, RelativePath)), Term;

load_externals(BasePath, {external_binary, RelativePath}) ->
  {ok, Data} = file:read_file(filename:join(BasePath, RelativePath)), Data;

load_externals(BasePath, {Identifier, Content})
  when is_atom(Identifier) and is_list(Content) ->
  {Identifier, [load_externals(BasePath, C) || C <- Content]};

load_externals(BasePath, {Identifier, Content}) when is_atom(Identifier) ->
  {Identifier, load_externals(BasePath, Content)};

load_externals(_, Term) -> Term.