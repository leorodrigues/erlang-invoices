%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.org
%%% @doc
%%%
%%% @end
%%% Created : 23. Sep 2021 21:37
%%%-------------------------------------------------------------------
-module(invoices_database_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").

-include("../src/invoices_records.hrl").

-export([all/0, find_all_invoices_1/1, find_invoice_by_id_1/1,
  find_invoice_by_id_2/1, invoice_exists_with_id_1/1,
  invoice_exists_with_id_2/1, insert_invoice_1/1, insert_invoice_2/1,
  update_invoice_1/1, update_invoice_2/1, delete_invoice_1/1,
  delete_invoice_2/1, generate_next_1/1]).

-export([init_per_testcase/2, end_per_testcase/2, init_per_suite/1,
  end_per_suite/1]).

-define(SERVER_NAME, invoices_database).

-import(test_support, [load_suite_data/1, load_case_data/3]).

generate_next_1(_) ->
  ct:comment("Should generate the next invoice number"),
  42 = invoices_database:generate_next(invoice_number).

find_all_invoices_1(Config) ->
  ct:comment("Should return all invoices in the database"),
  ExpectedList = load_case_data(Config, find_all_invoices_1, [invoice_list]),
  ExpectedList = invoices_database:find_all_invoices().

find_invoice_by_id_1(_) ->
  ct:comment("Should not find the given id in the database"),
  not_found = invoices_database:find_invoice_by_id(112358).

find_invoice_by_id_2(Config) ->
  ct:comment("Should find the specific invoice in the database"),
  ExpectedInvoice = load_case_data(Config, find_invoice_by_id_2, [invoice]),
  ExpectedInvoice = invoices_database:find_invoice_by_id(132134).

invoice_exists_with_id_1(_) ->
  ct:comment("There should be no invoice with id 112358"),
  false = invoices_database:invoice_exists_with_id(112358).

invoice_exists_with_id_2(_) ->
  ct:comment("There should an invoice with id 132134"),
  true = invoices_database:invoice_exists_with_id(132134).

insert_invoice_1(Config) ->
  ct:comment("Should save the new invoice and return it's id"),
  NewInvoice = load_case_data(Config, insert_invoice_1, [new_invoice]),
  132135 = invoices_database:insert_invoice(NewInvoice).

insert_invoice_2(Config) ->
  ct:comment("Should increment the id with each new call"),
  NewInvoice = load_case_data(Config, insert_invoice_2, [new_invoice]),
  132135 = invoices_database:insert_invoice(NewInvoice),
  132136 = invoices_database:insert_invoice(NewInvoice).

update_invoice_1(Config) ->
  ct:comment("Should update an invoice"),
  Original = load_case_data(Config, update_invoice_1, [original]),
  Updated = load_case_data(Config, update_invoice_1, [updated]),
  ok = invoices_database:update_invoice(change_po_number(Original)),
  Updated = invoices_database:find_invoice_by_id(Original#invoice.id).

update_invoice_2(Config) ->
  ct:comment("Should not alter the database if the invoice does not exist"),
  AlteredInvoice = load_case_data(Config, update_invoice_2, [invoice]),
  OriginalList = invoices_database:find_all_invoices(),
  ok = invoices_database:update_invoice(AlteredInvoice),
  OriginalList = invoices_database:find_all_invoices().

delete_invoice_1(_Config) ->
  ct:comment("Should do nothing with the database if the record is not found"),
  OriginalList = invoices_database:find_all_invoices(),
  ok = invoices_database:delete_invoice(10),
  OriginalList = invoices_database:find_all_invoices().

delete_invoice_2(_Config) ->
  ct:comment("Should remove the only invoice in the database"),
  [_] = invoices_database:find_all_invoices(),
  ok = invoices_database:delete_invoice(132134),
  [ ] = invoices_database:find_all_invoices().

%% --- Support functions --- --- --- --- --- --- --- --- --- --- --- --- --- ---

change_po_number(I = #invoice{ }) ->
  I#invoice{ po_number = <<"ABCD1234">> }.

%% --- Common test API functions --- --- --- --- --- --- --- --- --- --- --- ---

all() -> [find_all_invoices_1, find_invoice_by_id_1, find_invoice_by_id_2,
  invoice_exists_with_id_1, invoice_exists_with_id_2, insert_invoice_1,
  insert_invoice_2, update_invoice_1, update_invoice_2, delete_invoice_1,
  delete_invoice_2, generate_next_1].

init_per_testcase(_Case, Config) ->
  invoices_database:recreate(),
  invoices_database:load_from_text_file(get_dump_file_path(Config)),
  Config.

end_per_testcase(_Case, Config) ->
  Config.

init_per_suite(Config) ->
  gen_server:start({local, ?SERVER_NAME}, ?SERVER_NAME, [ ], [ ]),
  load_suite_data(Config).

end_per_suite(_Config) ->
  gen_server:stop(?SERVER_NAME, normal, 1000),
  ok.

get_dump_file_path(Config) ->
  filename:join(?config(data_dir, Config), "database_dump.terms").
