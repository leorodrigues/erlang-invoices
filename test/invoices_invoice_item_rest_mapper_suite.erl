%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leorodrigues.net
%%% @doc
%%%
%%% @end
%%% Created : 08. Oct 2021 19:56
%%%-------------------------------------------------------------------
-module(invoices_invoice_item_rest_mapper_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").
-include("../src/invoices_records.hrl").

-define(SERVER_NAME, invoices_invoice_item_rest_mapper).

-export([all/0, init_per_testcase/2, end_per_testcase/2, init_per_suite/1,
  end_per_suite/1]).

-export([from_json_1/1, from_json_2/1, to_json_1/1, to_json_2/1,
  to_json_3/1]).

-import(test_support, [load_suite_data/1, load_case_data/3]).

from_json_1(_) ->
  ct:comment("Should successfuly map an empty payload."),
  #invoice_item{ } = invoices_invoice_item_rest_mapper:from_json(<<"{}">>).

from_json_2(Config) ->
  ct:comment("Should map a full payload ignoring computed fields."),
  JsonBody = load_case_data(Config, from_json_2, [json_body]),
  TermBody = load_case_data(Config, from_json_2, [term_body]),
  TermBody = invoices_invoice_item_rest_mapper:from_json(JsonBody).

to_json_1(_) ->
  ct:comment("Should successfuly map an empty record to a json representation."),
  <<"{}">> = invoices_invoice_item_rest_mapper:to_json(#invoice_item{ }).

to_json_2(Config) ->
  ct:comment("Should successfuly map a full record to a json representation."),
  TermBody = load_case_data(Config, to_json_2, [term_body]),
  JsonBody = load_case_data(Config, to_json_2, [json_body]),
  JsonBody = invoices_invoice_item_rest_mapper:to_json(TermBody).

to_json_3(Config) ->
  ct:comment("Should successfuly map a list of records to a json representation."),
  TermBody = load_case_data(Config, to_json_3, [term_body]),
  JsonBody = load_case_data(Config, to_json_3, [json_body]),
  JsonBody = invoices_invoice_item_rest_mapper:to_json(TermBody).

all() -> [from_json_1, from_json_2, to_json_1, to_json_2, to_json_3].

init_per_testcase(_Case, Config) ->
  meck:new(invoices_database, [stub_all]),
  gen_server:start({local, ?SERVER_NAME}, ?SERVER_NAME, [ ], [ ]),
  Config.

end_per_testcase(_Case, Config) ->
  gen_server:stop(?SERVER_NAME, normal, 1000),
  meck:unload(invoices_database),
  Config.

init_per_suite(Config) ->
  application:start(jsx),
  load_suite_data(Config).

end_per_suite(_Config) ->
  application:stop(jsx),
  ok.

