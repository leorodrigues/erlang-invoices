{cover, "./cover.spec"}.
{suites, "./", [
  invoices_database_suite,
  invoices_use_cases_suite,
  invoices_crypto_suite,
  invoices_invoice_rest_mapper_suite,
  invoices_invoice_item_rest_mapper_suite
]}.
