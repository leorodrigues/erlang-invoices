{level, details}.
{incl_mods, [
  invoices_database,
  invoices_use_cases,
  invoices_crypto,
  invoices_invoice_rest_mapper,
  invoices_invoice_item_rest_mapper
]}.
